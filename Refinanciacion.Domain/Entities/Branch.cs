﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Branch
    {
        public Branch()
        {
            User = new HashSet<User>();
        }

        public decimal Id { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
