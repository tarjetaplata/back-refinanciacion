﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class User
    {
        public User()
        {
            TakeOffLoan = new HashSet<TakeOffLoan>();
        }

        public decimal Id { get; set; }
        public string Name { get; set; }
        public DateTime? LastEntryDate { get; set; }
        public string IsActive { get; set; }
        public DateTime? CreationDate { get; set; }
        public string IsLogged { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal RolesId { get; set; }
        public decimal StatusId { get; set; }
        public decimal BranchId { get; set; }

        public virtual Branch Branch { get; set; }
        public virtual Roles Roles { get; set; }
        public virtual Status Status { get; set; }
        public virtual ICollection<TakeOffLoan> TakeOffLoan { get; set; }
    }
}
