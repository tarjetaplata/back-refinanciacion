﻿using System;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Refinanciacion.Application.InstallmentCharge.Commands.CreateInstallmentCharge;
using Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList;

namespace Refinanciacion.Application.InstallmentCharge.Commands.CreateInstallmentCharge
{
    public class CreateInstallmentChargeCommandHandler : IRequestHandler<CreateInstallmentChargeCommand, object>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;

        public CreateInstallmentChargeCommandHandler(ModelContext context, ICurrentUserAccessor currentUserAccessor)
        {
            _context = context;
            _currentUserAccessor = currentUserAccessor;
        }

        public async Task<object> Handle(CreateInstallmentChargeCommand request, CancellationToken cancellationToken)
        {
            // Traemos el usuario que da el alta. Podemos simplificar guardando los datos del usuario en el JWT para no da.
            // Tienen que estar las APIS en Authorize para que funcione asi tenemos la session del usuario.
            // Con esto hacer el alta del history 
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());

            if (user == null)
                throw new NotFoundException(nameof(User), "usuario");


            ICollection<Domain.Entities.InstallmentCharge> collectionInstallmentCharge = null;

            request.listInstallmentCharge.ForEach(x => {
                Domain.Entities.InstallmentCharge installmentCharge = new Domain.Entities.InstallmentCharge();
                installmentCharge.Charge = x.charge;
                installmentCharge.Calculation = x.calculation;
                installmentCharge.CapitalPercent = x.capitalPercent;
                installmentCharge.FixedAmount = x.fixedAmount;
                installmentCharge.Iva = x.iva;

                collectionInstallmentCharge.Add(installmentCharge);
            });

            // Creamos el objeto a aguardar
            var entity = new InstallmentChargeValidity
            {
                FromDate = request.fromDate,
                ToDate = request.toDate,
                StampDateTime = DateTime.Now.Date,
                StampUser = user.Id,
                InstallmentCharge = collectionInstallmentCharge
            };

            // lo guardamos en la tabla
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;

        }


    }
}
