﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Threading.Tasks;

namespace Refinanciacion.Infrastructure.Security
{
    public interface IJwtTokenGenerator
    {
        //string CreateTokenAsync(string username, string rol);

        Task<string> CreateTokenAsync(string username, string rol);
    }
}
