﻿using Refinanciacion.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Text;
using System.Xml;

namespace Refinanciacion.Common
{
    public class UtilLoginTP
    {
        /// <summary>
        /// Llamada del web service de Login de tarjeta plata
        /// Devuelve un 0 cuando es correcto el usuario y contraseña... un 10 cuando tiene un error de autenticación.
        /// </summary>
        /// <param name="userName"> nombre del usuario para el active directory</param>
        /// <param name="password"> password del usuario de active directory</param>
        public static bool CallLoginWebService(string userName, string password, string url)
        {
            XmlDocument xmlResponse = new XmlDocument();
            Envelope response;

            var _url = url;
            //var _action = action;

            XmlDocument soapEnvelopeXml = CreateSoapEnvelope(userName, password);
            HttpWebRequest webRequest = CreateWebRequest(_url);
            InsertSoapEnvelopeIntoWebRequest(soapEnvelopeXml, webRequest);

            // comienzo de la solicitud asincrona del web service.
            IAsyncResult asyncResult = webRequest.BeginGetResponse(null, null);

            // suspende el hilo cuandoi la llamada se completa.
            asyncResult.AsyncWaitHandle.WaitOne();

            // obtener el response de la solicitud
            string soapResult;

            using (WebResponse webResponse = webRequest.EndGetResponse(asyncResult))
            {
                using (StreamReader rd = new StreamReader(webResponse.GetResponseStream()))
                {
                    soapResult = rd.ReadToEnd();
                }
            }

            xmlResponse.LoadXml(soapResult);

            using (TextReader sr = new StringReader(soapResult))
            {
                var serializer = new System.Xml.Serialization.XmlSerializer(typeof(Envelope));
                response = (Envelope)serializer.Deserialize(sr);
            }

            if (response != null)
            {
                if (response.Body.ValidateCredentialsResponse.ValidateCredentialsResult.Equals("0"))
                {
                    // loguear acá el usuario esta autenticado
                    return true;

                }
                else if (response.Body.ValidateCredentialsResponse.ValidateCredentialsResult.Equals("10"))
                {
                    // loguear acá el usuario no es valido
                    return false;
                }
            }
            else
            {
                // loguear acá que el response es nulo y que ocurrio un problema con el servicio de TP
                return false;
            }
            return false;

        }

        #region Request SOAP
        /// <summary>
        /// Creacion de la solicitud al SOAP
        /// </summary>
        /// <param name="url"></param>
        /// <param name="action"></param>
        /// <returns></returns>
        private static HttpWebRequest CreateWebRequest(string url)
        {
            HttpWebRequest webRequest = (HttpWebRequest)WebRequest.Create(url);
            //webRequest.Headers.Add("SOAPAction", action);
            webRequest.ContentType = "text/xml;charset=\"utf-8\"";
            webRequest.Accept = "text/xml";
            webRequest.Method = "POST";
            return webRequest;
        }
        #endregion

        #region Login Envelope 
        /// <summary>
        /// Creacion del body del SOAP para el login
        /// </summary>
        /// <param name="userName"></param>
        /// <param name="password"></param>
        /// <returns></returns>
        private static XmlDocument CreateSoapEnvelope(string userName, string password)
        {
            XmlDocument soapEnvelopeDocument = new XmlDocument();
            soapEnvelopeDocument.LoadXml(
                $@"<SOAP-ENV:Envelope xmlns:SOAP-ENV=""http://schemas.xmlsoap.org/soap/envelope/"">
                    <SOAP-ENV:Body>
                        <ValidateCredentials xmlns=""http://itau-relay.tarjetaplata.com.ar/"" SOAP-ENV:encodingStyle=""http://schemas.xmlsoap.org/soap/encoding/"">
                            <domain_user_id>{userName}</domain_user_id>
                            <domain_password>{password}</domain_password>
                        </ValidateCredentials>
                    </SOAP-ENV:Body>
                </SOAP-ENV:Envelope>");
            return soapEnvelopeDocument;
        }
        #endregion

        /// <summary>
        /// Método que mete el body adentro de la solicitud para el SOAP
        /// </summary>
        /// <param name="soapEnvelopeXml"></param>
        /// <param name="webRequest"></param>
        private static void InsertSoapEnvelopeIntoWebRequest(XmlDocument soapEnvelopeXml, HttpWebRequest webRequest)
        {
            using (Stream stream = webRequest.GetRequestStream())
            {
                soapEnvelopeXml.Save(stream);
            }
        }
    }
}
