﻿using MediatR;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Linq;

namespace Refinanciacion.Application.LoanParameters.Queries
{
    public class GetLoanParametersQueryHandler : IRequestHandler<GetLoanParametersQuery, LoanParametersLookupModel>
    {
        private readonly ModelContext _context;

        public GetLoanParametersQueryHandler(ModelContext context)
        {
            _context = context;
        }

        public async Task<LoanParametersLookupModel> Handle(GetLoanParametersQuery request, CancellationToken cancellationToken)
        {
            var parameters = _context.LoanParameters.SingleOrDefault();
            

            // armo el objeto para devolver al front
            LoanParametersLookupModel loanParametersLookupModel = new LoanParametersLookupModel();
            loanParametersLookupModel.FixedBranch = parameters.FixedBranch;
            loanParametersLookupModel.MaxDaysFirstInstallment = parameters.MaxDaysFirstInstallment;
            loanParametersLookupModel.MaxDaysPaymentVerify = parameters.MaxDaysPaymentVerify;
            loanParametersLookupModel.MinDaysRefinancing = parameters.MinDaysRefinancing;

            return loanParametersLookupModel;
        }
    }
}
