﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Roles.Queries.GetRolessList
{
    public class RolesListDto
    {
        public IList<RolesLookupModel> Roless { get; set; }
    }
}
