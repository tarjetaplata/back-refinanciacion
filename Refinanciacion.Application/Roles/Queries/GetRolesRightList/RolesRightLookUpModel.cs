﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Roles.Queries.GetRolesRightList
{
    public class RolesRightLookUpModel
    {
        public decimal IdRol { get; set; }
        public string Rol { get; set; }
        public decimal IdRight { get; set; }
        public string Right { get; set; }
    }
}
