﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class LoanTakeOffDebt
    {
        public decimal Id { get; set; }
        public decimal? FromDay { get; set; }
        public decimal? ToDay { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Intereses { get; set; }
        public decimal? Punitorio { get; set; }
        public decimal? Mora { get; set; }
        public decimal LoanTakeOffDebtValidityId { get; set; }

        public virtual LoanTakeOffDebtValidity LoanTakeOffDebtValidity { get; set; }
    }
}
