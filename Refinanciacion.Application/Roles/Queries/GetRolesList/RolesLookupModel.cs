﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Roles.Queries.GetRolessList
{
    public class RolesLookupModel
    {
        public decimal Id { get; set; }
        public string Rol { get; set; }
    }
}
