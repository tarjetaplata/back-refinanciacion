﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class RolesRights
    {
        public decimal Id { get; set; }
        public decimal RightsId { get; set; }
        public decimal RolesId { get; set; }

        public virtual Rights Rights { get; set; }
        public virtual Roles Roles { get; set; }
    }
}
