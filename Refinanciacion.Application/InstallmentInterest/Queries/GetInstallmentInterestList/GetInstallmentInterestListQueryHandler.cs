﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList
{
    public class GetInstallmentInterestListQueryHandler : IRequestHandler<GetInstallmentInterestListQuery, List<InstallmentInterestLookUpModel>>
    {
        private readonly ModelContext _context;
        private readonly ILogger<GetInstallmentInterestListQueryHandler> _logger;

        public GetInstallmentInterestListQueryHandler(ModelContext context, ILogger<GetInstallmentInterestListQueryHandler> getTargetListHandlerLogger)
        {
            _logger = getTargetListHandlerLogger;
            _context = context;
        }

        public async Task<List<InstallmentInterestLookUpModel>> Handle(GetInstallmentInterestListQuery request, CancellationToken cancellationToken)
        {
            // Devolver todos los installment charge con su respectiva validacion y que esten habilitados.
            List<InstallmentInterestsValidity> InstallmentInterestValidityList = new List<InstallmentInterestsValidity>();

            InstallmentInterestValidityList = await _context.InstallmentInterestsValidity
                    .Where(t => (t.StatusId == 0))
                    .Include(p => p.InstallmentInterests)
                    .ToListAsync();

            List<InstallmentInterestLookUpModel> listInstallmentInterestLookUpModel = new List<InstallmentInterestLookUpModel>();

            InstallmentInterestValidityList.ForEach(x =>
            {
                InstallmentInterestLookUpModel InstallmentInterestLookUpModel = new InstallmentInterestLookUpModel();
                InstallmentInterestLookUpModel.id = x.Id;
                InstallmentInterestLookUpModel.fromDate = x.FromDate;
                InstallmentInterestLookUpModel.toDate = x.ToDate;

                if (x.StatusId == 0)
                {
                    InstallmentInterestLookUpModel.status = true;
                }
                else {
                    InstallmentInterestLookUpModel.status = false;
                }

                InstallmentInterestLookUpModel.listInstallmentInterest = new List<InstallmentInterestModel>();

                x.InstallmentInterests.ToList().ForEach(y =>
                {
                    InstallmentInterestModel InstallmentInterestModel = new InstallmentInterestModel();

                    InstallmentInterestModel.installment = y.Installment;
                    InstallmentInterestModel.interest = y.Interests;

                    InstallmentInterestLookUpModel.listInstallmentInterest.Add(InstallmentInterestModel);

                });

                listInstallmentInterestLookUpModel.Add(InstallmentInterestLookUpModel);

            });

            return listInstallmentInterestLookUpModel;

        }
    }
}
