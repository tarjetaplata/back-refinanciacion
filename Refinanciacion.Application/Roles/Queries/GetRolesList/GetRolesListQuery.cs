﻿using System;
using System.Collections.Generic;
using System.Text;
using MediatR;

namespace Refinanciacion.Application.Roles.Queries.GetRolessList
{
    public class GetRolesListQuery : IRequest<List<RolesLookupModel>>
    {
    }
}
