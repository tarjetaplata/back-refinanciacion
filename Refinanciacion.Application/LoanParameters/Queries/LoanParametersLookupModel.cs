﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.LoanParameters.Queries
{
    public class LoanParametersLookupModel
    {
        public decimal? MinDaysRefinancing { get; set; }
        public decimal? MaxDaysFirstInstallment { get; set; }
        public decimal? MaxDaysPaymentVerify { get; set; }
        public string FixedBranch { get; set; }
    }
}
