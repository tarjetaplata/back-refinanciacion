﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Common.Models
{
    public class TargetUpload
    {
        public int Row { get; set; }
        public DateTime Date { get; set; }
        public string Branch { get; set; }
        public string Product { get; set; }
        public string Target { get; set; }

    }
}
