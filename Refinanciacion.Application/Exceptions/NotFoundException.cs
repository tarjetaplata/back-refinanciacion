﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Exceptions
{
    public class NotFoundException : Exception
    {
        public NotFoundException(string name, object key)
            : base($"Entidad \"{name}\" ({key}) no existe.")
        {
        }
    }
}
