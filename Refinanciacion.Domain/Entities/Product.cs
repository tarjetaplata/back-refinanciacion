﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Product
    {
        public Product()
        {
            LoanRefinancing = new HashSet<LoanRefinancing>();
            ProductConcept = new HashSet<ProductConcept>();
        }

        public decimal Id { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string Mnemonic { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<LoanRefinancing> LoanRefinancing { get; set; }
        public virtual ICollection<ProductConcept> ProductConcept { get; set; }
    }
}
