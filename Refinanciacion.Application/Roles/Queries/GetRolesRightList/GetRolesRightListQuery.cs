﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Roles.Queries.GetRolesRightList
{
    public class GetRolesRightListQuery : IRequest<List<RolesRightLookUpModel>>
    { 
    }
}
