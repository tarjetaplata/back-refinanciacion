﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Common.Exceptions
{
    public class ExcelParsingException : Exception
    {
        public ExcelParsingException(string mensaje)
            : base(mensaje)
        {
        }
    }

    public class ExcelDuplicatedEntityException : Exception
    {
        public ExcelDuplicatedEntityException(string branch, string date, string product)
            : base($"Sucursal {branch}, fecha {date}, producto {product},  tiene más de un registro en la tabla.")
        {
        }
    }

    public class ExcelPastTargetException : Exception
    {
        public ExcelPastTargetException(string branch, string date, string product)
            : base($"Sucursal {branch}, fecha {date}, producto {product}, se esta intentando editar un objetivo del pasado.")
        {
        }
    }

    public class ExcelBranchTypoException : Exception
    {
        public ExcelBranchTypoException(string branch, string row)
            : base($"Sucursal {branch} contiene error de tipeo o no existe en la base de datos. Fila {row}.")
        {
        }
    }

    public class ExcelDateTypoException : Exception
    {
        public ExcelDateTypoException(string branch, string date, string product)
            : base($"Sucursal {branch}, fecha {date}, producto {product}, el formato de la fecha no se reconoce.")
        {
        }
    }

    public class ExcelProductTypoException : Exception
    {
        public ExcelProductTypoException(string product, string row)
            : base($"Producto {product} contiene error de tipeo o no existe en la base de datos. Fila {row}.")
        {
        }
    }

    public class ExcelAmountTypoException : Exception
    {
        public ExcelAmountTypoException(string amount, string row)
            : base($"Objetivo {amount} contiene error de tipeo. No es un numero decimal válido. Fila {row}.")
        {
        }
    }

    public class ExcelAmountMustBePositiveNumberException : Exception
    {
        public ExcelAmountMustBePositiveNumberException(string amount, string row)
            : base($"Objetivo {amount} es menor que 0. No es un numero válido. Fila {row}.")
        {
        }
    }

    public class ExcelEmptyCellException : Exception
    {
        public ExcelEmptyCellException(string row)
            : base($"Hay celdas vacías en la fila {row}.")
        {
        }
    }

    public class ExcelEmptySheetException : Exception
    {
        public ExcelEmptySheetException()
            : base($"El fichero está vacío o no contiene datos.")
        {
        }
    }

    public class ExcelMalformedSheetException : Exception
    {
        public ExcelMalformedSheetException()
            : base($"La estructura del fichero es incorrecta. Verifique que en la primer fila se encuentran las cabeceras: Fecha, TODOS, Producto y Objetivo.")
        {
        }
    }
}
