﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Concept
    {
        public Concept()
        {
            ProductConcept = new HashSet<ProductConcept>();
        }

        public decimal Id { get; set; }
        public string ShortDesc { get; set; }

        public virtual ICollection<ProductConcept> ProductConcept { get; set; }
    }
}
