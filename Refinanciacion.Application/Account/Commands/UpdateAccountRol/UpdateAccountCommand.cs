﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Account.Commands.UpdateAccountRol
{
    public class UpdateAccountCommand : IRequest
    {
        public decimal id;
        public decimal rolId;
    }
}
