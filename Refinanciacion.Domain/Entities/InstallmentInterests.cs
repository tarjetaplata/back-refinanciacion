﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class InstallmentInterests
    {
        public decimal Id { get; set; }
        public decimal? Installment { get; set; }
        public decimal? Interests { get; set; }
        public decimal InstallmentIntsValidityId { get; set; }

        public virtual InstallmentInterestsValidity InstallmentIntsValidity { get; set; }
    }
}
