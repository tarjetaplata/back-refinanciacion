﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList
{
    public class GetInstallmentChargeListQuery : IRequest<List<InstallmentChargeLookUpModel>>
    {
    }
}