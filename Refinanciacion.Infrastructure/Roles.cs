﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Infrastructure
{
    public static class Roles
    {
        public const string Admin = "Admin";
        public const string Responsable_Sucursal = "Responsable Sucursal";
        public const string Encargado_Sucursal = "Encargado Sucursal";
        public const string Gerente_Comercial = "Gerente Comercial";
        public const string Director = "Director";

    }
}
