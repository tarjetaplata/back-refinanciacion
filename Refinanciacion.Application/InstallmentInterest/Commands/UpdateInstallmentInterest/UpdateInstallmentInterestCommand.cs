﻿using MediatR;
using Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentInterest.Commands.UpdateInstallmentInterest
{
    public class UpdateInstallmentInterestCommand : IRequest
    {
        public decimal id { get; set; }

        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public bool status { get; set; }

        public List<InstallmentInterestModel> listInstallmentInterest { get; set; }
    }
}
