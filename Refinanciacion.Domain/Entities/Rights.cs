﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Rights
    {
        public Rights()
        {
            RolesRights = new HashSet<RolesRights>();
        }

        public decimal Id { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<RolesRights> RolesRights { get; set; }
    }
}
