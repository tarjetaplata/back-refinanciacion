﻿using MediatR;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.LoanParameters.Commands;
using Refinanciacion.Application.LoanParameters.Queries;
using Refinanciacion.Application.Roles.Commands.UpdateRol;
using Refinanciacion.Controllers;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace back_refinanciacion.Controllers
{
    /// <summary>
    /// Devuelve la parametria de la aplicacion
    /// </summary>
    public class LoanParametersController : BaseController
    {

        [HttpGet]
        public async Task<ActionResult<List<LoanParametersLookupModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetLoanParametersQuery()));
        }
        

        // PUT: api/LoanParameters/5
        [HttpPut("right")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update([FromBody]UpdateLoanParametersCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

    }
}
