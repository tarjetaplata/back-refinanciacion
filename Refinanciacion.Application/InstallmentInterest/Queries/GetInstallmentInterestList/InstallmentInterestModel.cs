﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList
{
    public class InstallmentInterestModel
    {
        public decimal? installment { get; set; }

        public decimal? interest { get; set; }
    }
}
