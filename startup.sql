﻿-- Generado por Oracle SQL Developer Data Modeler 19.2.0.182.1216
--   en:        2019-08-16 03:52:53 ART
--   sitio:      Oracle Database 11g
--   tipo:      Oracle Database 11g



CREATE TABLE action (
    id           NUMBER NOT NULL,
    short_desc   VARCHAR2(255 CHAR),
    type         VARCHAR2(255 CHAR)
);

ALTER TABLE action ADD CONSTRAINT action_pk PRIMARY KEY ( id );

CREATE TABLE branch (
    id                NUMBER NOT NULL,
    short_desc        VARCHAR2(255 CHAR),
    long_desc         VARCHAR2(255 CHAR),
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

ALTER TABLE branch ADD CONSTRAINT branch_pk PRIMARY KEY ( id );

CREATE TABLE concept (
    id           NUMBER NOT NULL,
    short_desc   VARCHAR2(255)
);

ALTER TABLE concept ADD CONSTRAINT concept_pk PRIMARY KEY ( id );

CREATE TABLE grant_refinancing (
    id                           NUMBER NOT NULL,
    stamp_date_time              TIMESTAMP,
    refinancing_number           NUMBER,
    branch_refinancing           VARCHAR2(255),
    branch_user                  VARCHAR2(255),
    sum_installment              NUMBER,
    installment_value_capital    NUMBER,
    interes_percent              NUMBER,
    interes_amount               NUMBER,
    contingency_fund             NUMBER,
    assistance                   NUMBER,
    insurance                    NUMBER,
    total_amount_installment     NUMBER,
    due_date_first_installment   TIMESTAMP,
    total_amount_to_pay          NUMBER
);

COMMENT ON COLUMN grant_refinancing.stamp_date_time IS
    'fecha de creacion del nuevo credito';

COMMENT ON COLUMN grant_refinancing.refinancing_number IS
    'numero de refinanciacion';

COMMENT ON COLUMN grant_refinancing.branch_refinancing IS
    'sucursal refinanciacion';

COMMENT ON COLUMN grant_refinancing.branch_user IS
    'sucursal del usuario';

COMMENT ON COLUMN grant_refinancing.sum_installment IS
    'Cantidad de cuotas';

COMMENT ON COLUMN grant_refinancing.installment_value_capital IS
    'valor de la cuota capital';

COMMENT ON COLUMN grant_refinancing.interes_percent IS
    'porcentaje de interes';

COMMENT ON COLUMN grant_refinancing.interes_amount IS
    'Monto interes';

COMMENT ON COLUMN grant_refinancing.contingency_fund IS
    'Fondo de contingencia';

COMMENT ON COLUMN grant_refinancing.assistance IS
    'Asistencia';

COMMENT ON COLUMN grant_refinancing.insurance IS
    'Seguro';

COMMENT ON COLUMN grant_refinancing.total_amount_installment IS
    'Total a pagar por cuota';

COMMENT ON COLUMN grant_refinancing.due_date_first_installment IS
    'Fecha de vencimiento de la 1er cuota';

COMMENT ON COLUMN grant_refinancing.total_amount_to_pay IS
    'monto total a pagar';

ALTER TABLE grant_refinancing ADD CONSTRAINT grant_refinancing_pk PRIMARY KEY ( id );

CREATE TABLE historical_parameters (
    id                   NUMBER NOT NULL,
    creation_date        TIMESTAMP,
    user_id              NUMBER NOT NULL,
    action_id            NUMBER NOT NULL,
    loan_parameters_id   NUMBER NOT NULL
);

ALTER TABLE historical_parameters ADD CONSTRAINT historical_param_pk PRIMARY KEY ( id );

CREATE TABLE installment_charge (
    id                               NUMBER NOT NULL,
    charge                           VARCHAR2(255),
    calculation                      VARCHAR2(255),
    capital_percent                  NUMBER,
    fixed_amount                     NUMBER,
    iva                              NUMBER,
    installment_charge_validity_id   NUMBER NOT NULL
);

ALTER TABLE installment_charge ADD CONSTRAINT installment_charge_pk PRIMARY KEY ( id );

CREATE TABLE installment_charge_validity (
    id                NUMBER NOT NULL,
    from_date         TIMESTAMP,
    to_date           TIMESTAMP,
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER 
         --  ERROR: Column INSTALLMENT_CHARGE_VALIDITY.STATUS_ID check constraint name length exceeds maximum allowed length(30) 
        CONSTRAINT nnc_ints_valty_status_id NOT NULL
);

ALTER TABLE installment_charge_validity ADD CONSTRAINT installt_ints_validity_pk PRIMARY KEY ( id );

CREATE TABLE installment_interests (
    id                             NUMBER,
    installment                    NUMBER,
    interests                      NUMBER,
    installment_ints_validity_id   NUMBER NOT NULL
);

CREATE TABLE installment_interests_validity (
    id                NUMBER NOT NULL,
    from_date         TIMESTAMP,
    to_date           TIMESTAMP,
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

ALTER TABLE installment_interests_validity ADD CONSTRAINT installment_interests_pk PRIMARY KEY ( id );

CREATE TABLE loan_parameters (
    id                           NUMBER NOT NULL,
    min_days_refinancing         NUMBER,
    max_days_first_installment   NUMBER,
    max_days_payment_verify      NUMBER,
    fixed_branch                 VARCHAR2(255),
    status_date                  TIMESTAMP,
    stamp_date_time              TIMESTAMP,
    stamp_user                   NUMBER,
    status_id                    NUMBER NOT NULL
);

COMMENT ON COLUMN loan_parameters.min_days_refinancing IS
    '[Datos Generales] : Cantidad de dias de mora minimo para que pueda acceder a un plan de refinanciación.';

COMMENT ON COLUMN loan_parameters.max_days_first_installment IS
    '[Datos generales] : Cantidad de dias corridos para el vencimiento de la primer cuota.';

COMMENT ON COLUMN loan_parameters.max_days_payment_verify IS
    '[Control de deuda] : Cantidad de dias corridos para la verificación del pago de la deuda después del vencimiento.';

COMMENT ON COLUMN loan_parameters.fixed_branch IS
    '[Control de deuda] : Si esta vacio se toma el del usuario sino se considera que es fijo ';

ALTER TABLE loan_parameters ADD CONSTRAINT loan_parameters_pk PRIMARY KEY ( id );

CREATE TABLE loan_refinancing (
    id                     NUMBER NOT NULL,
    loan_id                NUMBER,
    status_date            TIMESTAMP,
    stamp_date_time        TIMESTAMP,
    status_id              NUMBER NOT NULL,
    id_take_off_loan       NUMBER NOT NULL,
    id_product             NUMBER NOT NULL,
    id_grant_refinancing   NUMBER
);

COMMENT ON COLUMN loan_refinancing.status_date IS
    'Fecha en que se modificó por ultima vez el estado del credito';

COMMENT ON COLUMN loan_refinancing.stamp_date_time IS
    'Fecha que se creo el credito refinanciado';

COMMENT ON COLUMN loan_refinancing.status_id IS
    'Estado del crédito refinanciado correspondera si está Vigente, si se Revirtió por no pagar la 1ra cuota o si se encuentra Pendiente del pago de la 1er cuota'
    ;

COMMENT ON COLUMN loan_refinancing.id_grant_refinancing IS
    'id de la tabla de los creditos nuevos creados en technisys a partir de los prestamos';

ALTER TABLE loan_refinancing ADD CONSTRAINT refinancing_pk PRIMARY KEY ( id );

CREATE TABLE loan_take_off_debt (
    id                               NUMBER NOT NULL,
    from_day                         NUMBER,
    to_day                           NUMBER,
    capital                          NUMBER,
    intereses                        NUMBER,
    punitorio                        NUMBER,
    mora                             NUMBER,
    loan_take_off_debt_validity_id   NUMBER NOT NULL
);

COMMENT ON COLUMN loan_take_off_debt.id IS
    'Clave primaria de la entidad';

COMMENT ON COLUMN loan_take_off_debt.from_day IS
    'Dias de mora';

COMMENT ON COLUMN loan_take_off_debt.to_day IS
    'Dias de mora';

ALTER TABLE loan_take_off_debt ADD CONSTRAINT loan_toff_debt_valty_pk PRIMARY KEY ( id );

CREATE TABLE loan_take_off_debt_validity (
    id                NUMBER NOT NULL,
    from_date         TIMESTAMP,
    to_date           TIMESTAMP,
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

COMMENT ON COLUMN loan_take_off_debt_validity.id IS
    'Clave primaria de la entidad';

COMMENT ON COLUMN loan_take_off_debt_validity.from_date IS
    'Fecha de vigencia desde';

COMMENT ON COLUMN loan_take_off_debt_validity.to_date IS
    'Fecha de vigencia hasta';

COMMENT ON COLUMN loan_take_off_debt_validity.status_date IS
    'Fecha de modificacion de estado';

COMMENT ON COLUMN loan_take_off_debt_validity.stamp_date_time IS
    'Fecha que impacta ';

COMMENT ON COLUMN loan_take_off_debt_validity.stamp_user IS
    'Usuario que modifica';

COMMENT ON COLUMN loan_take_off_debt_validity.status_id IS
    'Habilitado o no hablitado';

ALTER TABLE loan_take_off_debt_validity ADD CONSTRAINT loan_take_off_debt_pk PRIMARY KEY ( id );

CREATE TABLE product (
    id                NUMBER NOT NULL,
    short_desc        VARCHAR2(255 CHAR),
    long_desc         VARCHAR2(255 CHAR),
    mnemonic          VARCHAR2(255 CHAR),
    status_date       DATE,
    stamp_date_time   DATE,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

ALTER TABLE product ADD CONSTRAINT product_pk PRIMARY KEY ( id );

CREATE TABLE product_concept (
    id           NUMBER NOT NULL,
    id_product   NUMBER NOT NULL,
    id_concept   NUMBER NOT NULL
);

ALTER TABLE product_concept ADD CONSTRAINT product_concept_pk PRIMARY KEY ( id );

CREATE TABLE rights (
    id                NUMBER NOT NULL,
    short_desc        VARCHAR2(255 CHAR),
    long_desc         VARCHAR2(255 CHAR),
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

ALTER TABLE rights ADD CONSTRAINT permissions_pk PRIMARY KEY ( id );

CREATE TABLE roles (
    id                NUMBER NOT NULL,
    short_desc        VARCHAR2(255 CHAR),
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    status_id         NUMBER NOT NULL
);

ALTER TABLE roles ADD CONSTRAINT roles_pk PRIMARY KEY ( id );

CREATE TABLE roles_rights (
    id          NUMBER NOT NULL,
    rights_id   NUMBER NOT NULL,
    roles_id    NUMBER NOT NULL
);

ALTER TABLE roles_rights ADD CONSTRAINT roles_rights_pk PRIMARY KEY ( id );

CREATE TABLE status (
    id                NUMBER NOT NULL,
    mnemonic          VARCHAR2(255 CHAR),
    short_desc        VARCHAR2(255 CHAR),
    long_desc         VARCHAR2(255 CHAR),
    status_id         VARCHAR2(255 CHAR),
    status_date       TIMESTAMP,
    stamp_user        NUMBER,
    stamp_date_time   TIMESTAMP
);

ALTER TABLE status ADD CONSTRAINT status_pk PRIMARY KEY ( id );

CREATE TABLE take_off_loan (
    id          NUMBER NOT NULL,
    capital     NUMBER,
    interes     NUMBER,
    punitorio   NUMBER,
    mora        NUMBER,
    id_user     NUMBER NOT NULL
);

ALTER TABLE take_off_loan ADD CONSTRAINT take_off_loan_pk PRIMARY KEY ( id );

CREATE TABLE "USER" (
    id                NUMBER NOT NULL,
    name              VARCHAR2(255 CHAR),
    last_entry_date   TIMESTAMP,
    is_active         CHAR(1),
    creation_date     TIMESTAMP,
    is_logged         CHAR(1),
    status_date       TIMESTAMP,
    stamp_date_time   TIMESTAMP,
    stamp_user        NUMBER,
    roles_id          NUMBER NOT NULL,
    status_id         NUMBER NOT NULL,
    branch_id         NUMBER NOT NULL
);

ALTER TABLE "USER" ADD CONSTRAINT user_pk PRIMARY KEY ( id );

ALTER TABLE branch
    ADD CONSTRAINT branch_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE product_concept
    ADD CONSTRAINT con_concept_fk FOREIGN KEY ( id_concept )
        REFERENCES concept ( id );

ALTER TABLE historical_parameters
    ADD CONSTRAINT hist_para_action_fk FOREIGN KEY ( action_id )
        REFERENCES action ( id );

ALTER TABLE historical_parameters
    ADD CONSTRAINT hist_para_loa_para_fk FOREIGN KEY ( loan_parameters_id )
        REFERENCES loan_parameters ( id );

ALTER TABLE installment_interests
    ADD CONSTRAINT ins_ins_validity_fk FOREIGN KEY ( installment_ints_validity_id )
        REFERENCES installment_interests_validity ( id );

ALTER TABLE installment_interests_validity
    ADD CONSTRAINT ins_int_sta_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE installment_charge_validity
    ADD CONSTRAINT ins_int_val_stat_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE installment_charge
    ADD CONSTRAINT int_cha_ins_cha_val_fk FOREIGN KEY ( installment_charge_validity_id )
        REFERENCES installment_charge_validity ( id );

ALTER TABLE loan_parameters
    ADD CONSTRAINT loan_parameters_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE loan_refinancing
    ADD CONSTRAINT loan_refinancing_product_fk FOREIGN KEY ( id_product )
        REFERENCES product ( id );

ALTER TABLE loan_take_off_debt_validity
    ADD CONSTRAINT loan_take_off_debt_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE loan_take_off_debt
    ADD CONSTRAINT off_debt_validity_fk FOREIGN KEY ( loan_take_off_debt_validity_id )
        REFERENCES loan_take_off_debt_validity ( id );

ALTER TABLE product_concept
    ADD CONSTRAINT pro_product_fk FOREIGN KEY ( id_product )
        REFERENCES product ( id );

ALTER TABLE product
    ADD CONSTRAINT product_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE loan_refinancing
    ADD CONSTRAINT refinancing_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE loan_refinancing
    ADD CONSTRAINT refng_off_loan_fk FOREIGN KEY ( id_take_off_loan )
        REFERENCES take_off_loan ( id );

ALTER TABLE rights
    ADD CONSTRAINT rights_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE roles_rights
    ADD CONSTRAINT roles_rights_rights_fk FOREIGN KEY ( rights_id )
        REFERENCES rights ( id );

ALTER TABLE roles_rights
    ADD CONSTRAINT roles_rights_roles_fk FOREIGN KEY ( roles_id )
        REFERENCES roles ( id );

ALTER TABLE roles
    ADD CONSTRAINT roles_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );

ALTER TABLE take_off_loan
    ADD CONSTRAINT tk_off_user_fk FOREIGN KEY ( id_user )
        REFERENCES "USER" ( id );

ALTER TABLE "USER"
    ADD CONSTRAINT user_branch_fk FOREIGN KEY ( branch_id )
        REFERENCES branch ( id );

ALTER TABLE "USER"
    ADD CONSTRAINT user_roles_fk FOREIGN KEY ( roles_id )
        REFERENCES roles ( id );

ALTER TABLE "USER"
    ADD CONSTRAINT user_status_fk FOREIGN KEY ( status_id )
        REFERENCES status ( id );



-- Informe de Resumen de Oracle SQL Developer Data Modeler: 
-- 
-- CREATE TABLE                            21
-- CREATE INDEX                             0
-- ALTER TABLE                             44
-- CREATE VIEW                              0
-- ALTER VIEW                               0
-- CREATE PACKAGE                           0
-- CREATE PACKAGE BODY                      0
-- CREATE PROCEDURE                         0
-- CREATE FUNCTION                          0
-- CREATE TRIGGER                           0
-- ALTER TRIGGER                            0
-- CREATE COLLECTION TYPE                   0
-- CREATE STRUCTURED TYPE                   0
-- CREATE STRUCTURED TYPE BODY              0
-- CREATE CLUSTER                           0
-- CREATE CONTEXT                           0
-- CREATE DATABASE                          0
-- CREATE DIMENSION                         0
-- CREATE DIRECTORY                         0
-- CREATE DISK GROUP                        0
-- CREATE ROLE                              0
-- CREATE ROLLBACK SEGMENT                  0
-- CREATE SEQUENCE                          0
-- CREATE MATERIALIZED VIEW                 0
-- CREATE MATERIALIZED VIEW LOG             0
-- CREATE SYNONYM                           0
-- CREATE TABLESPACE                        0
-- CREATE USER                              0
-- 
-- DROP TABLESPACE                          0
-- DROP DATABASE                            0
-- 
-- REDACTION POLICY                         0
-- 
-- ORDS DROP SCHEMA                         0
-- ORDS ENABLE SCHEMA                       0
-- ORDS ENABLE OBJECT                       0
-- 
-- ERRORS                                   1
-- WARNINGS                                 0



