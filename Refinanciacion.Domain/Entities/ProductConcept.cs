﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class ProductConcept
    {
        public decimal Id { get; set; }
        public decimal IdProduct { get; set; }
        public decimal IdConcept { get; set; }

        public virtual Concept IdConceptNavigation { get; set; }
        public virtual Product IdProductNavigation { get; set; }
    }
}
