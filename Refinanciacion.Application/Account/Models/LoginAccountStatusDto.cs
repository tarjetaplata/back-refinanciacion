﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Account.Models
{
    public class LoginAccountStatusDto
    {
        public LoginAccountStatusDto(string status, string message)
        {
            Status = status;
            Message = message;
        }

        public string Status { get; set; }
        public string Message { get; set; }
    }
}
