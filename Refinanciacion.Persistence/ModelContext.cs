﻿using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Metadata;
using Refinanciacion.Domain.Entities;

namespace Refinanciacion.Persistence
{
    public partial class ModelContext : DbContext
    {
        public ModelContext()
        {
        }

        public ModelContext(DbContextOptions<ModelContext> options)
            : base(options)
        {
        }

        public virtual DbSet<Domain.Entities.Action> Action { get; set; }
        public virtual DbSet<Branch> Branch { get; set; }
        public virtual DbSet<Concept> Concept { get; set; }
        public virtual DbSet<GrantRefinancing> GrantRefinancing { get; set; }
        public virtual DbSet<HistoricalParameters> HistoricalParameters { get; set; }
        public virtual DbSet<InstallmentCharge> InstallmentCharge { get; set; }
        public virtual DbSet<InstallmentChargeValidity> InstallmentChargeValidity { get; set; }
        public virtual DbSet<InstallmentInterests> InstallmentInterests { get; set; }
        public virtual DbSet<InstallmentInterestsValidity> InstallmentInterestsValidity { get; set; }
        public virtual DbSet<LoanParameters> LoanParameters { get; set; }
        public virtual DbSet<LoanRefinancing> LoanRefinancing { get; set; }
        public virtual DbSet<LoanTakeOffDebt> LoanTakeOffDebt { get; set; }
        public virtual DbSet<LoanTakeOffDebtValidity> LoanTakeOffDebtValidity { get; set; }
        public virtual DbSet<Product> Product { get; set; }
        public virtual DbSet<ProductConcept> ProductConcept { get; set; }
        public virtual DbSet<Rights> Rights { get; set; }
        public virtual DbSet<Roles> Roles { get; set; }
        public virtual DbSet<RolesRights> RolesRights { get; set; }
        public virtual DbSet<Status> Status { get; set; }
        public virtual DbSet<TakeOffLoan> TakeOffLoan { get; set; }
        public virtual DbSet<User> User { get; set; }

        protected override void OnModelCreating(ModelBuilder modelBuilder)
        {
            modelBuilder.HasAnnotation("ProductVersion", "2.2.6-servicing-10079")
                .HasAnnotation("Relational:DefaultSchema", "REFIDEV");

            modelBuilder.Entity<Domain.Entities.Action>(entity =>
            {
                entity.ToTable("ACTION");

                entity.HasIndex(e => e.Id)
                    .HasName("ACTION_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.Type)
                    .HasColumnName("TYPE")
                    .HasColumnType("VARCHAR2(255)");
            });

            modelBuilder.Entity<Branch>(entity =>
            {
                entity.ToTable("BRANCH");

                entity.HasIndex(e => e.Id)
                    .HasName("BRANCH_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LongDesc)
                    .HasColumnName("LONG_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Branch)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("BRANCH_STATUS_FK");
            });

            modelBuilder.Entity<Concept>(entity =>
            {
                entity.ToTable("CONCEPT");

                entity.HasIndex(e => e.Id)
                    .HasName("CONCEPT_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");
            });

            modelBuilder.Entity<GrantRefinancing>(entity =>
            {
                entity.ToTable("GRANT_REFINANCING");

                entity.HasIndex(e => e.Id)
                    .HasName("GRANT_REFINANCING_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Assistance)
                    .HasColumnName("ASSISTANCE")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.BranchRefinancing)
                    .HasColumnName("BRANCH_REFINANCING")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.BranchUser)
                    .HasColumnName("BRANCH_USER")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.ContingencyFund)
                    .HasColumnName("CONTINGENCY_FUND")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.DueDateFirstInstallment)
                    .HasColumnName("DUE_DATE_FIRST_INSTALLMENT")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.InstallmentValueCapital)
                    .HasColumnName("INSTALLMENT_VALUE_CAPITAL")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Insurance)
                    .HasColumnName("INSURANCE")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.InteresAmount)
                    .HasColumnName("INTERES_AMOUNT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.InteresPercent)
                    .HasColumnName("INTERES_PERCENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.RefinancingNumber)
                    .HasColumnName("REFINANCING_NUMBER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.SumInstallment)
                    .HasColumnName("SUM_INSTALLMENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.TotalAmountInstallment)
                    .HasColumnName("TOTAL_AMOUNT_INSTALLMENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.TotalAmountToPay)
                    .HasColumnName("TOTAL_AMOUNT_TO_PAY")
                    .HasColumnType("NUMBER");
            });

            modelBuilder.Entity<HistoricalParameters>(entity =>
            {
                entity.ToTable("HISTORICAL_PARAMETERS");

                entity.HasIndex(e => e.Id)
                    .HasName("HISTORICAL_PARAM_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ActionId)
                    .HasColumnName("ACTION_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.LoanParametersId)
                    .HasColumnName("LOAN_PARAMETERS_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.UserId)
                    .HasColumnName("USER_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Action)
                    .WithMany(p => p.HistoricalParameters)
                    .HasForeignKey(d => d.ActionId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("HIST_PARA_ACTION_FK");

                entity.HasOne(d => d.LoanParameters)
                    .WithMany(p => p.HistoricalParameters)
                    .HasForeignKey(d => d.LoanParametersId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("HIST_PARA_LOA_PARA_FK");
            });

            modelBuilder.Entity<InstallmentCharge>(entity =>
            {
                entity.ToTable("INSTALLMENT_CHARGE");

                entity.HasIndex(e => e.Id)
                    .HasName("INSTALLMENT_CHARGE_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Calculation)
                    .HasColumnName("CALCULATION")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.CapitalPercent)
                    .HasColumnName("CAPITAL_PERCENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Charge)
                    .HasColumnName("CHARGE")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.FixedAmount)
                    .HasColumnName("FIXED_AMOUNT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.InstallmentChargeValidityId)
                    .HasColumnName("INSTALLMENT_CHARGE_VALIDITY_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Iva)
                    .HasColumnName("IVA")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.InstallmentChargeValidity)
                    .WithMany(p => p.InstallmentCharge)
                    .HasForeignKey(d => d.InstallmentChargeValidityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("INT_CHA_INS_CHA_VAL_FK");
            });

            modelBuilder.Entity<InstallmentChargeValidity>(entity =>
            {
                entity.ToTable("INSTALLMENT_CHARGE_VALIDITY");

                entity.HasIndex(e => e.Id)
                    .HasName("INSTALLT_INTS_VALIDITY_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FromDate)
                    .HasColumnName("FROM_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ToDate)
                    .HasColumnName("TO_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.InstallmentChargeValidity)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("INS_INT_VAL_STAT_FK");
            });

            modelBuilder.Entity<InstallmentInterests>(entity =>
            {
                entity.ToTable("INSTALLMENT_INTERESTS");

                entity.HasIndex(e => e.Id)
                    .HasName("INSTALLMENT_INTERESTS_PK1")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Installment)
                    .HasColumnName("INSTALLMENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.InstallmentIntsValidityId)
                    .HasColumnName("INSTALLMENT_INTS_VALIDITY_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Interests)
                    .HasColumnName("INTERESTS")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.InstallmentIntsValidity)
                    .WithMany(p => p.InstallmentInterests)
                    .HasForeignKey(d => d.InstallmentIntsValidityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("INSNT_ITS_FK1");
            });

            modelBuilder.Entity<InstallmentInterestsValidity>(entity =>
            {
                entity.ToTable("INSTALLMENT_INTERESTS_VALIDITY");

                entity.HasIndex(e => e.Id)
                    .HasName("INSTALLMENT_INTERESTS_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FromDate)
                    .HasColumnName("FROM_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ToDate)
                    .HasColumnName("TO_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.InstallmentInterestsValidity)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("INS_INT_STA_FK");
            });

            modelBuilder.Entity<LoanParameters>(entity =>
            {
                entity.ToTable("LOAN_PARAMETERS");

                entity.HasIndex(e => e.Id)
                    .HasName("LOAN_PARAMETERS_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FixedBranch)
                    .HasColumnName("FIXED_BRANCH")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.MaxDaysFirstInstallment)
                    .HasColumnName("MAX_DAYS_FIRST_INSTALLMENT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.MaxDaysPaymentVerify)
                    .HasColumnName("MAX_DAYS_PAYMENT_VERIFY")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.MinDaysRefinancing)
                    .HasColumnName("MIN_DAYS_REFINANCING")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LoanParameters)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LOAN_PARAMETERS_STATUS_FK");
            });

            modelBuilder.Entity<LoanRefinancing>(entity =>
            {
                entity.ToTable("LOAN_REFINANCING");

                entity.HasIndex(e => e.Id)
                    .HasName("REFINANCING_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdGrantRefinancing)
                    .HasColumnName("ID_GRANT_REFINANCING")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("ID_PRODUCT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdTakeOffLoan)
                    .HasColumnName("ID_TAKE_OFF_LOAN")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LoanId)
                    .HasColumnName("LOAN_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.LoanRefinancing)
                    .HasForeignKey(d => d.IdProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LOAN_REFINANCING_PRODUCT_FK");

                entity.HasOne(d => d.IdTakeOffLoanNavigation)
                    .WithMany(p => p.LoanRefinancing)
                    .HasForeignKey(d => d.IdTakeOffLoan)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("REFNG_OFF_LOAN_FK");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LoanRefinancing)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("REFINANCING_STATUS_FK");
            });

            modelBuilder.Entity<LoanTakeOffDebt>(entity =>
            {
                entity.ToTable("LOAN_TAKE_OFF_DEBT");

                entity.HasIndex(e => e.Id)
                    .HasName("LOAN_TOFF_DEBT_VALTY_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Capital)
                    .HasColumnName("CAPITAL")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FromDay)
                    .HasColumnName("FROM_DAY")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Intereses)
                    .HasColumnName("INTERESES")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LoanTakeOffDebtValidityId)
                    .HasColumnName("LOAN_TAKE_OFF_DEBT_VALIDITY_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Mora)
                    .HasColumnName("MORA")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Punitorio)
                    .HasColumnName("PUNITORIO")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ToDay)
                    .HasColumnName("TO_DAY")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.LoanTakeOffDebtValidity)
                    .WithMany(p => p.LoanTakeOffDebt)
                    .HasForeignKey(d => d.LoanTakeOffDebtValidityId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("OFF_DEBT_VALIDITY_FK");
            });

            modelBuilder.Entity<LoanTakeOffDebtValidity>(entity =>
            {
                entity.ToTable("LOAN_TAKE_OFF_DEBT_VALIDITY");

                entity.HasIndex(e => e.Id)
                    .HasName("LOAN_TAKE_OFF_DEBT_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.FromDate)
                    .HasColumnName("FROM_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ToDate)
                    .HasColumnName("TO_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.LoanTakeOffDebtValidity)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("LOAN_TAKE_OFF_DEBT_STATUS_FK");
            });

            modelBuilder.Entity<Product>(entity =>
            {
                entity.ToTable("PRODUCT");

                entity.HasIndex(e => e.Id)
                    .HasName("PRODUCT_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LongDesc)
                    .HasColumnName("LONG_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.Mnemonic)
                    .HasColumnName("MNEMONIC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("DATE");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("DATE");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Product)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("PRODUCT_STATUS_FK");
            });

            modelBuilder.Entity<ProductConcept>(entity =>
            {
                entity.ToTable("PRODUCT_CONCEPT");

                entity.HasIndex(e => e.Id)
                    .HasName("PRODUCT_CONCEPT_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdConcept)
                    .HasColumnName("ID_CONCEPT")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdProduct)
                    .HasColumnName("ID_PRODUCT")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.IdConceptNavigation)
                    .WithMany(p => p.ProductConcept)
                    .HasForeignKey(d => d.IdConcept)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("CON_CONCEPT_FK");

                entity.HasOne(d => d.IdProductNavigation)
                    .WithMany(p => p.ProductConcept)
                    .HasForeignKey(d => d.IdProduct)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("PRO_PRODUCT_FK");
            });

            modelBuilder.Entity<Rights>(entity =>
            {
                entity.ToTable("RIGHTS");

                entity.HasIndex(e => e.Id)
                    .HasName("PERMISSIONS_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LongDesc)
                    .HasColumnName("LONG_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Rights)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("RIGHTS_STATUS_FK");
            });

            modelBuilder.Entity<Roles>(entity =>
            {
                entity.ToTable("ROLES");

                entity.HasIndex(e => e.Id)
                    .HasName("ROLES_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.Roles)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ROLES_STATUS_FK");
            });

            modelBuilder.Entity<RolesRights>(entity =>
            {
                entity.ToTable("ROLES_RIGHTS");

                entity.HasIndex(e => e.Id)
                    .HasName("ROLES_RIGHTS_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.RightsId)
                    .HasColumnName("RIGHTS_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.RolesId)
                    .HasColumnName("ROLES_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Rights)
                    .WithMany(p => p.RolesRights)
                    .HasForeignKey(d => d.RightsId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ROLES_RIGHTS_RIGHTS_FK");

                entity.HasOne(d => d.Roles)
                    .WithMany(p => p.RolesRights)
                    .HasForeignKey(d => d.RolesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("ROLES_RIGHTS_ROLES_FK");
            });

            modelBuilder.Entity<Status>(entity =>
            {
                entity.ToTable("STATUS");

                entity.HasIndex(e => e.Id)
                    .HasName("STATUS_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.LongDesc)
                    .HasColumnName("LONG_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.Mnemonic)
                    .HasColumnName("MNEMONIC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.ShortDesc)
                    .HasColumnName("SHORT_DESC")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("VARCHAR2(255)");
            });

            modelBuilder.Entity<TakeOffLoan>(entity =>
            {
                entity.ToTable("TAKE_OFF_LOAN");

                entity.HasIndex(e => e.Id)
                    .HasName("TAKE_OFF_LOAN_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Capital)
                    .HasColumnName("CAPITAL")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.IdUser)
                    .HasColumnName("ID_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Interes)
                    .HasColumnName("INTERES")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Mora)
                    .HasColumnName("MORA")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.Punitorio)
                    .HasColumnName("PUNITORIO")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.IdUserNavigation)
                    .WithMany(p => p.TakeOffLoan)
                    .HasForeignKey(d => d.IdUser)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("TK_OFF_USER_FK");
            });

            modelBuilder.Entity<User>(entity =>
            {
                entity.ToTable("USER");

                entity.HasIndex(e => e.Id)
                    .HasName("USER_PK")
                    .IsUnique();

                entity.Property(e => e.Id)
                    .HasColumnName("ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.BranchId)
                    .HasColumnName("BRANCH_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.CreationDate)
                    .HasColumnName("CREATION_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.IsActive)
                    .HasColumnName("IS_ACTIVE")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.IsLogged)
                    .HasColumnName("IS_LOGGED")
                    .HasColumnType("CHAR(1)");

                entity.Property(e => e.LastEntryDate)
                    .HasColumnName("LAST_ENTRY_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.Name)
                    .HasColumnName("NAME")
                    .HasColumnType("VARCHAR2(255)");

                entity.Property(e => e.RolesId)
                    .HasColumnName("ROLES_ID")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StampDateTime)
                    .HasColumnName("STAMP_DATE_TIME")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StampUser)
                    .HasColumnName("STAMP_USER")
                    .HasColumnType("NUMBER");

                entity.Property(e => e.StatusDate)
                    .HasColumnName("STATUS_DATE")
                    .HasColumnType("TIMESTAMP(6)");

                entity.Property(e => e.StatusId)
                    .HasColumnName("STATUS_ID")
                    .HasColumnType("NUMBER");

                entity.HasOne(d => d.Branch)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.BranchId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("USER_BRANCH_FK");

                entity.HasOne(d => d.Roles)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.RolesId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("USER_ROLES_FK");

                entity.HasOne(d => d.Status)
                    .WithMany(p => p.User)
                    .HasForeignKey(d => d.StatusId)
                    .OnDelete(DeleteBehavior.ClientSetNull)
                    .HasConstraintName("USER_STATUS_FK");
            });
        }
    }
}
