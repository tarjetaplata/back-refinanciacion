﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList
{
    public class GetInstallmentChargeListQueryHandler : IRequestHandler<GetInstallmentChargeListQuery, List<InstallmentChargeLookUpModel>>
    {
        private readonly ModelContext _context;
        private readonly ILogger<GetInstallmentChargeListQueryHandler> _logger;

        public GetInstallmentChargeListQueryHandler(ModelContext context, ILogger<GetInstallmentChargeListQueryHandler> getTargetListHandlerLogger)
        {
            _logger = getTargetListHandlerLogger;
            _context = context;
        }

        public async Task<List<InstallmentChargeLookUpModel>> Handle(GetInstallmentChargeListQuery request, CancellationToken cancellationToken)
        {
            // Devolver todos los installment charge con su respectiva validacion y que esten habilitados.
            List<InstallmentChargeValidity> installmentChargeValidityList = new List<InstallmentChargeValidity>();

            installmentChargeValidityList = await _context.InstallmentChargeValidity
                    .Where(t => (t.StatusId == 0))
                    .Include(p => p.InstallmentCharge)
                    .ToListAsync();

            List<InstallmentChargeLookUpModel> listInstallmentChargeLookUpModel = new List<InstallmentChargeLookUpModel>();

            installmentChargeValidityList.ForEach(x =>
            {
                InstallmentChargeLookUpModel installmentChargeLookUpModel = new InstallmentChargeLookUpModel();
                installmentChargeLookUpModel.id = x.Id;
                installmentChargeLookUpModel.fromDate = x.FromDate;
                installmentChargeLookUpModel.toDate = x.ToDate;

                if (x.StatusId == 0)
                {
                    installmentChargeLookUpModel.status = true;
                }
                else {
                    installmentChargeLookUpModel.status = false;
                }

                installmentChargeLookUpModel.listInstallmentCharge = new List<InstallmentChargeModel>();

                x.InstallmentCharge.ToList().ForEach(y =>
                {
                    InstallmentChargeModel installmentChargeModel = new InstallmentChargeModel();

                    installmentChargeModel.charge = y.Charge;
                    installmentChargeModel.calculation = y.Calculation;
                    installmentChargeModel.capitalPercent = y.CapitalPercent;
                    installmentChargeModel.iva = y.Iva;
                    installmentChargeModel.fixedAmount = y.FixedAmount;

                    installmentChargeLookUpModel.listInstallmentCharge.Add(installmentChargeModel);

                });

                listInstallmentChargeLookUpModel.Add(installmentChargeLookUpModel);

            });

            return listInstallmentChargeLookUpModel;

        }
    }
}
