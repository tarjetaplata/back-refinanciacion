﻿using CsvHelper;
using Microsoft.AspNetCore.Http;
using NPOI.SS.UserModel;
using NPOI.SS.Util;
using NPOI.XSSF.UserModel;
using Refinanciacion.Common.Models;
using System;
using System.Collections.Generic;
using System.IO;
using System.Reflection;
using System.Text;
using Refinanciacion.Common.Exceptions;

namespace Refinanciacion.Common
{
    public abstract class UtilExcel
    {

        public void WriteTableContent(List<object> _rows, int _sheetIndex, int _rowIndex, int _columnIndex, IWorkbook destWorkbook)
        {
            ISheet sheet = destWorkbook.GetSheetAt(_sheetIndex);
            IRow row;  // title row
            ICell cell;
            int auxRowColumn = 0; //Auxiliar row column. Increments by row...

            foreach (object rowData in _rows)
            {
                row = sheet.GetRow(_rowIndex) ?? sheet.CreateRow(_rowIndex);

                auxRowColumn = _columnIndex;
                foreach (PropertyInfo propertyInfo in rowData.GetType().GetProperties(
                                        BindingFlags.Public
                                        | BindingFlags.Instance))
                {
                    cell = row.GetCell(auxRowColumn, MissingCellPolicy.CREATE_NULL_AS_BLANK); // con esta linea devuelve la celda con el formato del template
                    //cell = row.CreateCell(auxRowColumn, CellType.String); // con esta linea crea una nueva celda pero pierde el formato 
                    cell.SetCellValue((propertyInfo.GetValue(rowData, null) ?? "").ToString());

                    auxRowColumn++;
                }

                _rowIndex++;
            }

        }

        public void getCellData(IWorkbook workbook, string range)
        {
            var sheet = workbook.GetSheetAt(0);
            var rangee = range;
            // ejemplo = "E3:H8";
            var cellRange = CellRangeAddress.ValueOf(rangee);

            for (var i = cellRange.FirstRow; i <= cellRange.LastRow; i++)
            {
                var row = sheet.GetRow(i);
                for (var j = cellRange.FirstColumn; j <= cellRange.LastColumn; j++)
                {
                    if (j == 5) continue;

                }

            }
        }

        public static int monthNumber(string month)
        {
            switch (month.ToLower())
            {
                case "ene":
                    return 1;
                case "feb":
                    return 2;
                case "mar":
                    return 3;
                case "abr":
                    return 4;
                case "may":
                    return 5;
                case "jun":
                    return 6;
                case "jul":
                    return 7;
                case "ago":
                    return 8;
                case "sep":
                    return 9;
                case "oct":
                    return 10;
                case "nov":
                    return 11;
                case "div":
                    return 12;

            }
            return 0;
        }

        public static DateTime getDateFromString(string record)
        {
            return new DateTime(DateTime.Now.Year, monthNumber(record.Split("-")[1]), Convert.ToInt32(record.Split("-")[0]));
        }

        public static List<TargetUpload> TargetUploadsFromCsv(Stream targetStream)
        {
            var targetUploadsList = new List<TargetUpload>();
            using (var reader = new StreamReader(targetStream))
            using (var csv = new CsvReader(reader))
            {
                List<TargetUploadCsv> records = new List<TargetUploadCsv>();
                try
                {
                    records.AddRange(csv.GetRecords<TargetUploadCsv>());
                }
                catch (Exception)
                {

                    throw new ExcelMalformedSheetException();
                }
                
                int row = 1;

                foreach (var record in records)
                {
                    if(record.Fecha.Length > 0 && record.Objetivo.Length > 0 && record.Producto.Length > 0 && record.TODOS.Length > 0)
                    {
                        TargetUpload carga = new TargetUpload()
                        {
                            
                            Branch = record.TODOS,
                            Product = record.Producto,
                            Row = row + 1
                        };
                        try
                        {
                            carga.Date = getDateFromString(record.Fecha);
                        }
                        catch (Exception)
                        {
                            throw new ExcelDateTypoException(record.TODOS, record.Fecha, record.Producto);
                        }
                        try
                        {
                            carga.Target = Convert.ToDecimal(record.Objetivo).ToString();
                        }
                        catch (Exception)
                        {

                            throw new ExcelAmountTypoException(record.Objetivo, (row + 1).ToString());
                        }
                        targetUploadsList.Add(carga);
                    }
                    
                    row++;
                }
                return targetUploadsList;
            }

        }

        public static List<TargetUpload> TargetUploadsFromXls(Stream targetStream)
        {
            var targetUploadsList = new List<TargetUpload>();
            NPOI.HSSF.UserModel.HSSFWorkbook hSSFWorkbook = new NPOI.HSSF.UserModel.HSSFWorkbook(targetStream);
            NPOI.SS.UserModel.ISheet sheet = hSSFWorkbook.GetSheetAt(0);
            for (int row = 1; row <= sheet.LastRowNum; row++)
            {
                IRow sheetRow = sheet.GetRow(row);
                if (sheetRow != null)
                {
                    var target = (UtilExcel.Row2TargetUpload(sheetRow, row + 1));
                    if (target!= null)
                        targetUploadsList.Add(target);
                }
                else
                    throw new ExcelEmptySheetException();
            }
            return targetUploadsList;
        }

        public static TargetUpload Row2TargetUpload(IRow sheetRow, int rowNumber)
        {
            if (sheetRow.Cells.Count > 3 && rowNumber != 0)
            {
                if (sheetRow.Cells[0].ColumnIndex != 0
                    || sheetRow.Cells[1].ColumnIndex != 1
                    || sheetRow.Cells[2].ColumnIndex != 2
                    || sheetRow.Cells[3].ColumnIndex != 3)
                {
                    throw new ExcelEmptyCellException(rowNumber.ToString());
                }
                if (sheetRow.Cells[1].ToString().Length > 0 && sheetRow.Cells[2].ToString().Length > 0)
                {
                    TargetUpload carga = new TargetUpload()
                    {
                        Branch = sheetRow.Cells[1].ToString(),
                        Product = sheetRow.Cells[2].ToString(),
                        Row = rowNumber
                    };
                    try
                    {
                        carga.Date = sheetRow.Cells[0].DateCellValue;
                    }
                    catch (Exception)
                    {

                        throw new ExcelDateTypoException(sheetRow.Cells[1].ToString(), sheetRow.Cells[0].ToString(), sheetRow.Cells[2].ToString());
                    }
                    try
                    {
                        carga.Target = sheetRow.Cells[3].NumericCellValue.ToString();

                    }
                    catch (Exception)
                    {

                        throw new ExcelAmountTypoException(sheetRow.Cells[3].ToString(), (rowNumber).ToString());
                    }

                    return carga;
                }
                else
                    return null;
            }
            else
                return null;

        }

        public static List<TargetUpload> TargetUploadsFromXlsx(Stream targetStream)
        {
            var targetUploadsList = new List<TargetUpload>();
            var hssfworkbook = new XSSFWorkbook(targetStream);
            ISheet sheet = hssfworkbook.GetSheetAt(0);
            IRow headerRow = sheet.GetRow(0);
            System.Collections.IEnumerator rows = sheet.GetRowEnumerator();
            int colCount = headerRow.LastCellNum;
            int rowCount = sheet.LastRowNum;
            bool skipReadingHeaderRow = rows.MoveNext();
            int row = 1;
            while (rows.MoveNext())
            {
                IRow sheetRow = (XSSFRow)rows.Current;
                if (sheetRow != null)
                {
                    var target = UtilExcel.Row2TargetUpload(sheetRow, row + 1);
                    if(target != null)
                        targetUploadsList.Add(target);
                }
                else
                    throw new ExcelEmptySheetException();

                row++;
            }
            hssfworkbook = null;
            sheet = null;
            return targetUploadsList;
        }

        public static List<TargetUpload> TargetUploadsFromExcel(IFormFile file)
        {
            using (MemoryStream targetStream = new MemoryStream())
            {
                Stream sourceStream = file.OpenReadStream();
                try
                {
                    byte[] buffer = new byte[sourceStream.Length + 1];
                    int read = 0;
                    while ((read = sourceStream.Read(buffer, 0, buffer.Length)) > 0)
                    {
                        targetStream.Write(buffer, 0, read);
                    }
                    sourceStream.Position = 0;
                    if (file.FileName.EndsWith(".xlsx"))
                    {
                        return TargetUploadsFromXlsx(sourceStream);
                    }
                    else if (file.FileName.EndsWith(".xls"))
                    {
                        return TargetUploadsFromXls(sourceStream);
                    }
                    else if (file.FileName.EndsWith(".csv"))
                    {
                        return TargetUploadsFromCsv(sourceStream);
                    }
                }
                catch (Exception ex)
                {
                    throw ex;
                }
            }
            return null;
        }
    }
}
