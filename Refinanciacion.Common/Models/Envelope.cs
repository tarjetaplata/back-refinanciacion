﻿using System;
using System.Collections.Generic;
using System.Text;
using System.Xml.Serialization;

namespace Refinanciacion.Common.Models
{
    [XmlRoot(ElementName = "Envelope", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Envelope
    {
        [XmlElement(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
        public Body Body { get; set; }
        [XmlAttribute(AttributeName = "soap", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Soap { get; set; }
        [XmlAttribute(AttributeName = "xsi", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsi { get; set; }
        [XmlAttribute(AttributeName = "xsd", Namespace = "http://www.w3.org/2000/xmlns/")]
        public string Xsd { get; set; }
    }

    [XmlRoot(ElementName = "ValidateCredentialsResponse", Namespace = "http://itau-relay.tarjetaplata.com.ar/")]
    public class ValidateCredentialsResponse
    {
        [XmlElement(ElementName = "ValidateCredentialsResult", Namespace = "http://itau-relay.tarjetaplata.com.ar/")]
        public string ValidateCredentialsResult { get; set; }
        [XmlAttribute(AttributeName = "xmlns")]
        public string Xmlns { get; set; }
    }

    [XmlRoot(ElementName = "Body", Namespace = "http://schemas.xmlsoap.org/soap/envelope/")]
    public class Body
    {
        [XmlElement(ElementName = "ValidateCredentialsResponse", Namespace = "http://itau-relay.tarjetaplata.com.ar/")]
        public ValidateCredentialsResponse ValidateCredentialsResponse { get; set; }
    }
}
