﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.LoanParameters.Commands
{
    public class UpdateLoanParametersCommand : IRequest
    {
        public decimal MinDaysRefinancing { get; set; }
        public decimal MaxDaysFirstInstallment { get; set; }
        public decimal MaxDaysPaymentVerify { get; set; }
        public string FixedBranch { get; set; }
    }
}
