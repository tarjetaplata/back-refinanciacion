﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Infrastructure
{
    public class AppSettings
    {
        public string SecretKey { get; set; }

        public ExternalServices externalServices { get; set; }
    }

    public class ExternalServices
    {
        public string UrlLoginTP { get; set; }

        public string Action { get; set; }
    }
}
