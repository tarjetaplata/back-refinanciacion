﻿using System;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.InstallmentCharge.Commands.UpdateInstallmentCharge
{
    public class UpdateInstallmentChargeCommandHandler : IRequestHandler<UpdateInstallmentChargeCommand, Unit>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;

        public UpdateInstallmentChargeCommandHandler(ModelContext context,
            IMediator mediator, ICurrentUserAccessor currentUserAccessor)
        {
            _currentUserAccessor = currentUserAccessor;
            _context = context;
        }

        public async Task<Unit> Handle(UpdateInstallmentChargeCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());

            var entity = await _context.InstallmentChargeValidity
                .SingleAsync(c => c.Id == request.id, cancellationToken);


            if (entity == null)
            {
                throw new NotFoundException(nameof(InstallmentChargeValidity), request.id);
            }

            entity.StatusId = 2;
            entity.StatusDate = DateTime.Now;
            entity.StampUser = user.Id;

            ICollection<Domain.Entities.InstallmentCharge> collectionInstallmentCharge = null;

            request.listInstallmentCharge.ForEach(x => {
                Domain.Entities.InstallmentCharge installmentCharge = new Domain.Entities.InstallmentCharge();
                installmentCharge.Charge = x.charge;
                installmentCharge.Calculation = x.calculation;
                installmentCharge.CapitalPercent = x.capitalPercent;
                installmentCharge.FixedAmount = x.fixedAmount;
                installmentCharge.Iva = x.iva;

                collectionInstallmentCharge.Add(installmentCharge);
            });

            // Creamos el objeto a aguardar
            var newEntity = new InstallmentChargeValidity
            {
                FromDate = request.fromDate,
                ToDate = request.toDate,
                StampDateTime = DateTime.Now.Date,
                StampUser = user.Id,
                InstallmentCharge = collectionInstallmentCharge
            };

            _context.InstallmentChargeValidity.Update(entity);
            _context.InstallmentChargeValidity.Add(newEntity);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
