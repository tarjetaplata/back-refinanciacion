﻿using MediatR;
using Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentCharge.Commands.UpdateInstallmentCharge
{
    public class UpdateInstallmentChargeCommand : IRequest
    {
        public decimal id { get; set; }

        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public bool status { get; set; }

        public List<InstallmentChargeModel> listInstallmentCharge { get; set; }
    }
}
