﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Common
{
    public class SortOrderDto
    {
        public string field;
        public string direction;

        public string PairAsSqlExpression
        {
            get
            {
                return $"{field} {direction}";
            }
        }
    }
}
