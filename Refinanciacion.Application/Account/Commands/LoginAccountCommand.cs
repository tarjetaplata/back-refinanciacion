﻿using MediatR;
using Refinanciacion.Application.Account.Models;

namespace Refinanciacion.Application.Account.Commands
{
    public class LoginAccountCommand : IRequest<object>
    {
        public string UserName { get; set; }

        public string Password { get; set; }
    }
}
