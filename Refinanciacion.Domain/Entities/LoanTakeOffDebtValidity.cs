﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class LoanTakeOffDebtValidity
    {
        public LoanTakeOffDebtValidity()
        {
            LoanTakeOffDebt = new HashSet<LoanTakeOffDebt>();
        }

        public decimal Id { get; set; }
        public DateTime? FromDate { get; set; }
        public DateTime? ToDate { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<LoanTakeOffDebt> LoanTakeOffDebt { get; set; }
    }
}
