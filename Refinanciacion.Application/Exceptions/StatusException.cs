﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Exceptions
{
    public class StatusException : Exception
    {
       public StatusException(string name, string status)
            : base($"La Entidad \"{name}\" tiene status ({status}) comuniquese con el area de sistemas si necesita realizar un cambio de estado sobre la entidad.")
        {
        }
    }
}
