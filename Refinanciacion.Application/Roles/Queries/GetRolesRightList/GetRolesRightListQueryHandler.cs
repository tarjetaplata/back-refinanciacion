﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.Roles.Queries.GetRolesRightList
{
    public class GetRolesRightListQueryHandler : IRequestHandler<GetRolesRightListQuery, List<RolesRightLookUpModel>>
    {
        private readonly ModelContext _context;

        public GetRolesRightListQueryHandler(ModelContext context)
        {
            _context = context;
        }

        public async Task<List<RolesRightLookUpModel>> Handle(GetRolesRightListQuery request, CancellationToken cancellationToken)
        {
            var roles = await _context.RolesRights
                .Include(x => x.Rights)
                .Include(x => x.Roles)
                .ToListAsync();
            
            List<RolesRightLookUpModel> listRolesRightLookUpModel = new List<RolesRightLookUpModel>();

            roles.ForEach(x =>
            {
                RolesRightLookUpModel rolesRightLookupModel = new RolesRightLookUpModel();
                rolesRightLookupModel.IdRol = x.RolesId;
                rolesRightLookupModel.Rol = x.Roles.ShortDesc;

                rolesRightLookupModel.IdRight = x.RightsId;
                rolesRightLookupModel.Right = x.Rights.ShortDesc;

                listRolesRightLookUpModel.Add(rolesRightLookupModel);
            });

            return listRolesRightLookUpModel;
        }
    }
}
