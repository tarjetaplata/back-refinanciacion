﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using MediatR;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.InstallmentCharge.Commands.CreateInstallmentCharge;
using Refinanciacion.Application.InstallmentCharge.Commands.UpdateInstallmentCharge;
using Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList;
using Refinanciacion.Controllers;

namespace back_refinanciacion.Controllers
{
    public class InstallmentChargeController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<InstallmentChargeLookUpModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetInstallmentChargeListQuery()));
        }

        // POST: api/installmentcharge
        [HttpPost]
        public async Task<object> Create([FromBody]CreateInstallmentChargeCommand command)
        {
            return await Mediator.Send(command);
        }

        // PUT api/installmentcharge/5 // actualiza un installmentCharge con id = 5 (por ejemplo)
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(string id, [FromBody]UpdateInstallmentChargeCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}
