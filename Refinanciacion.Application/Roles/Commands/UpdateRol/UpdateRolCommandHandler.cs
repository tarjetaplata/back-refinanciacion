﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.Roles.Commands.UpdateRol
{
    public class UpdateRolCommandHandler : IRequestHandler<UpdateRolCommand, Unit>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;


        public UpdateRolCommandHandler(ModelContext context, ICurrentUserAccessor currentUserAccessor)
        {
            _currentUserAccessor = currentUserAccessor;
            _context = context;
        }

        public async Task<Unit> Handle(UpdateRolCommand request, CancellationToken cancellationToken)
        {
            // Usuario que modifica el rol
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());

            var roles = await _context.RolesRights
                .Include(x => x.Rights)
                .Include(x => x.Roles)
                .Where(rr => rr.RolesId == request.idRol)
                .ToListAsync();

            // buscamos si el id de Right existe dentro del mismo para saber si es baja sino se da de alta
            var entity = roles.Find(x => x.RightsId == request.idRight && x.RolesId == request.idRol);

            if (entity != null) {
                _context.RolesRights.Remove(entity);
                await _context.SaveChangesAsync(cancellationToken);

            } else {

                RolesRights rightRoles = new RolesRights();
                rightRoles.RightsId = request.idRight;
                rightRoles.RolesId = request.idRol;
                _context.RolesRights.Add(rightRoles);
                await _context.SaveChangesAsync(cancellationToken);

            }
            return Unit.Value;
        }
    }
}
