﻿
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;


namespace Refinanciacion.Application.Roles.Queries.GetRolessList
{
    public class GetRolesListQueryHandler : IRequestHandler<GetRolesListQuery, List<RolesLookupModel>>
    {
        private readonly ModelContext _context;

        public GetRolesListQueryHandler(ModelContext context)
        {
            _context = context;
        }

        public async Task<List<RolesLookupModel>> Handle(GetRolesListQuery request, CancellationToken cancellationToken)
        {
            // Version 1.0 me traigo todos los roles existentes con sus permisos
            //var roles = await _context.Roles
            //    .Include(p => p.RolesRights)
            //    .ThenInclude(c => c.Rights)
            //    .ToListAsync();
            var roles = await _context.Roles
                .ToListAsync();


            // armo el objeto para devolver al front
            List<RolesLookupModel> listRolesLookUpModel = new List<RolesLookupModel>();

            roles.ForEach(x =>
            {
                RolesLookupModel rolesLookupModel = new RolesLookupModel();
                rolesLookupModel.Id = x.Id;
                rolesLookupModel.Rol = x.ShortDesc;

                listRolesLookUpModel.Add(rolesLookupModel);
            });

            return listRolesLookUpModel;
        }
    }
}
