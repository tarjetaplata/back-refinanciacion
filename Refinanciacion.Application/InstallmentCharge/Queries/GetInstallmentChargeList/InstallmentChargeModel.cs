﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList
{
    public class InstallmentChargeModel
    {
        public string charge { get; set; }

        public string calculation { get; set; }

        public decimal? capitalPercent { get; set; }

        public decimal? fixedAmount { get; set; }

        public decimal? iva { get; set; }

    }
}
