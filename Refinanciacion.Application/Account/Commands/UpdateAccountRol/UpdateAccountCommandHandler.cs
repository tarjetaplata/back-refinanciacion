﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.Account.Commands.UpdateAccountRol
{
    public class UpdateAccountCommandHandler : IRequestHandler<UpdateAccountCommand, Unit>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;

        public UpdateAccountCommandHandler(ModelContext context, ICurrentUserAccessor currentUserAccessor)
        {
            _currentUserAccessor = currentUserAccessor;
            _context = context;
        }

        public async Task<Unit> Handle(UpdateAccountCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());
            
            // busco la entidad de usuario a modificar
            var entity = await _context.User
                .SingleAsync(c => c.Id == request.id, cancellationToken);

            if (entity == null)
                throw new NotFoundException(nameof(User), request.id);
            
            entity.StampDateTime = DateTime.Now;
            entity.StampUser = user.Id;
            entity.RolesId = request.rolId;

            _context.User.Update(entity);

            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
