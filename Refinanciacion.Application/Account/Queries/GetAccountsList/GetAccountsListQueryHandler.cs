﻿using AutoMapper.QueryableExtensions;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Persistence;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.Account.Queries.GetAccountsList
{
    public class GetAccountsListQueryHandler : IRequestHandler<GetAccountsListQuery, List<AccountLookupModel>>
    {
        private readonly ModelContext _context;
        
        public GetAccountsListQueryHandler(ModelContext context)
        {
            _context = context;
        }

        public async Task<List<AccountLookupModel>> Handle(GetAccountsListQuery request, CancellationToken cancellationToken)
        {
            var user = await _context.User.ToListAsync();

            //var listBranch = _context.ExcelBranch.ToDictionary(x => x.Id, x => x.ShortDesc);
            
            List<AccountLookupModel> listAccountLookUpModel = new List<AccountLookupModel>();

            user.ForEach(x =>
            {
                AccountLookupModel accountLookupModel = new AccountLookupModel();
                accountLookupModel.id = x.Id;
                accountLookupModel.userName = x.Name;
                accountLookupModel.sucursal = null;
                accountLookupModel.rolId = x.RolesId;
                listAccountLookUpModel.Add(accountLookupModel);
            });

            return listAccountLookUpModel;
        }
    }
}
