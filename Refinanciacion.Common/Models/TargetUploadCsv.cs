﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Common.Models
{
    public class TargetUploadCsv
    {
        public string Fecha { get; set; }
        public string TODOS { get; set; }
        public string Producto { get; set; }
        public string Objetivo { get; set; }

    }
}
