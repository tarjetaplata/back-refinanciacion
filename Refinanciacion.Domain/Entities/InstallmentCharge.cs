﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class InstallmentCharge
    {
        public decimal Id { get; set; }
        public string Charge { get; set; }
        public string Calculation { get; set; }
        public decimal? CapitalPercent { get; set; }
        public decimal? FixedAmount { get; set; }
        public decimal? Iva { get; set; }
        public decimal InstallmentChargeValidityId { get; set; }

        public virtual InstallmentChargeValidity InstallmentChargeValidity { get; set; }
    }
}
