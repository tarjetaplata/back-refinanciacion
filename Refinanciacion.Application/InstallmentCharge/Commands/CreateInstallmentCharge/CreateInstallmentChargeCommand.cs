﻿using Refinanciacion.Application.InstallmentCharge.Queries.GetInstallmentChargeList;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentCharge.Commands.CreateInstallmentCharge
{
    public class CreateInstallmentChargeCommand : IRequest<object>
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public bool status { get; set; }

        public List<InstallmentChargeModel> listInstallmentCharge { get; set; }
}
}
