﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Account.Models
{
    public class UserDto
    {

        public UserDto(string name, string rol, string token,string sucursal ,List<string> rights)
        {
            this.userName = name;
            this.role = rol;
            this.token = token;
            this.rights = rights;
            this.sucursal = sucursal;
        }

        public string userName { get; set; }
        public string role { get; set; }
        public string token { get; set; }
        public string sucursal { get; set; }
        public List<string> rights { get; set; }
    }
}
