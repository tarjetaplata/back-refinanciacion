﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class HistoricalParameters
    {
        public decimal Id { get; set; }
        public DateTime? CreationDate { get; set; }
        public decimal UserId { get; set; }
        public decimal ActionId { get; set; }
        public decimal LoanParametersId { get; set; }

        public virtual Action Action { get; set; }
        public virtual LoanParameters LoanParameters { get; set; }
    }
}
