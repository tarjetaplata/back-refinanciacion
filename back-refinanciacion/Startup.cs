﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Logging;
using Microsoft.Extensions.Options;
using Refinanciacion.Persistence;
using MediatR;
using Swashbuckle.AspNetCore.Swagger;
using Refinanciacion.Application.Account.Commands;
using System.Reflection;
using Refinanciacion.Infrastructure.Security;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.IdentityModel.Tokens;
using System.Text;
using Refinanciacion.Infrastructure;
using Refinanciacion.Application.Roles.Queries.GetRolessList;
using Refinanciacion.Models;
using Serilog;
using System.IO;
using MediatR.Pipeline;
using Refinanciacion.Application.Infrastructure;
using Microsoft.AspNetCore.Http;
using Refinanciacion.Application.Account.Queries.GetAccountsList;
using System.Globalization;
using Serilog.Events;
using Refinanciacion;

namespace back_refinanciacion
{
    public class Startup
    {
        public Startup(IHostingEnvironment env)
        {

            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: false, reloadOnChange: true)
                .AddJsonFile($"appsettings.{env.EnvironmentName}.json", optional: true)
                .AddEnvironmentVariables();

            Configuration = builder.Build();

            CurrentEnvironment = env;

            // Ojo aca con el Path Combine si no se crea el archivo puede ser un problema de esto o que el
            // usuario de ISS no tenga permisos en la carpeta.
            Log.Logger = new LoggerConfiguration()
                .MinimumLevel.Debug()
                .WriteTo.RollingFile(
                    Path.Combine(env.ContentRootPath, @"Logs\Refinanciacion.{Date}.txt"), // path format
                    LogEventLevel.Verbose,
                    "{Timestamp:yyyy-MM-dd HH:mm:ss.fff zzz} [REFI] [{Level}] [{RequestId}] - {Message}{NewLine}{Exception}",
                    null,
                    1000000,
                    2,
                    null,
                    false,
                    true,
                    TimeSpan.ParseExact("00:00:01", @"hh\:mm\:ss", CultureInfo.InvariantCulture))
                .CreateLogger();
        }

        public IConfiguration Configuration { get; }

        public IHostingEnvironment CurrentEnvironment { get; }

        public void ConfigureServices(IServiceCollection services)
        {
            services.AddCors();
            services.AddMediatR();

            // Add Settings
            services.Configure<AppSettings>(Configuration.GetSection("AppSettings"));
            services.AddScoped(typeof(IPipelineBehavior<,>), typeof(DBContextTransactionPipelineBehavior<,>));
            // Add MediatR

            // Pipelines
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPreProcessorBehavior<,>));
            services.AddTransient(typeof(IPipelineBehavior<,>), typeof(RequestPerformanceBehaviour<,>));

            // Commands
            services.AddMediatR(typeof(LoginAccountCommandHandler).GetTypeInfo().Assembly);

            // Queries
            services.AddMediatR(typeof(GetRolesListQueryHandler).GetTypeInfo().Assembly);
            services.AddMediatR(typeof(GetAccountsListQueryHandler).GetTypeInfo().Assembly);
            services.AddMvc();

            // Add Swagger
            services.AddSwaggerGen(x =>
            {
                x.AddSecurityDefinition("Bearer", new ApiKeyScheme
                {
                    In = "header",
                    Description = "Please insert JWT with Bearer into field",
                    Name = "Authorization",
                    Type = "apiKey"
                });

                x.AddSecurityRequirement(new Dictionary<string, IEnumerable<string>>
                {
                    {"Bearer", new string[] { }}
                });
                x.SwaggerDoc("v1", new Info { Title = "Refinanciacion API", Version = "v1" });
                x.CustomSchemaIds(y => y.FullName);
                x.DocInclusionPredicate((version, apiDescription) => true);
                x.TagActionsBy(y => new List<string>()
                {
                    y.GroupName
                });
                x.OperationFilter<FileUploadOperation>(); //Register File Upload Operation Filter
            });

            // Add DbContext using Oracle Provider 11g
            services.AddDbContext<ModelContext>(options =>
                options.UseOracle(Configuration.GetConnectionString("DefaultConnection"),
                b => b.UseOracleSQLCompatibility("11")));

            // Add JWT SECURITY ROLE BASED
            var appSettingsSection = Configuration.GetSection("AppSettings");
            var appSettings = appSettingsSection.Get<AppSettings>();
            // para el otro ejemplo que ya funciona usar este
            //var key = Encoding.ASCII.GetBytes(appSettings.SecretKey);
            // para el de thinkster usar este 
            var key = appSettings.SecretKey;

            //services.AddAuthentication(x =>
            //{
            //    x.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
            //    x.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
            //})
            //.AddJwtBearer(x =>
            //{
            //    x.RequireHttpsMetadata = false;
            //    x.SaveToken = true;
            //    x.TokenValidationParameters = new TokenValidationParameters
            //    {
            //        ValidateIssuerSigningKey = true,
            //        IssuerSigningKey = new SymmetricSecurityKey(key),
            //        ValidateIssuer = false,
            //        ValidateAudience = false
            //    };
            //});

            services.AddScoped<IJwtTokenGenerator, JwtTokenGenerator>();
            services.AddScoped<ICurrentUserAccessor, CurrentUserAccessor>();
            services.AddSingleton<IHttpContextAccessor, HttpContextAccessor>();

            services.AddJwt(key);

            // Add ID


        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseSwagger(c =>
            {
                c.RouteTemplate = "swagger/{documentName}/swagger.json";
            });

            app.UseSwaggerUI(c =>
            {
                c.SwaggerEndpoint("/swagger/v1/swagger.json", "Refinanciacion v1.0");
            });

            app.UseCors(x => x
                .AllowAnyOrigin()
                .AllowAnyMethod()
                .AllowAnyHeader());

            app.UseAuthentication();

            app.UseMvc();
        }
    }
}
