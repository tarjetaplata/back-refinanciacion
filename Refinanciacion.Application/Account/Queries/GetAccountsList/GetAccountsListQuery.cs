﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Account.Queries.GetAccountsList
{
    public class GetAccountsListQuery : IRequest<List<AccountLookupModel>>
    {
    }
}
