﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList
{
    public class InstallmentInterestLookUpModel
    {
        public decimal id { get; set; }

        public DateTime? fromDate { get; set; }

        public DateTime? toDate { get; set; }

        public bool status { get; set; }

        public List<InstallmentInterestModel> listInstallmentInterest { get; set; }

    }
}
