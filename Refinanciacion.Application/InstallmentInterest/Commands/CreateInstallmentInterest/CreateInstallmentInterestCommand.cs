﻿using Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList;
using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentInterest.Commands.CreateInstallmentInterest
{
    public class CreateInstallmentInterestCommand : IRequest<object>
    {
        public DateTime fromDate { get; set; }

        public DateTime toDate { get; set; }

        public bool status { get; set; }

        public List<InstallmentInterestModel> listInstallmentInterest { get; set; }
}
}
