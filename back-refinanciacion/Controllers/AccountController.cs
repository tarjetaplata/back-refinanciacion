﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.Account.Commands;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.Net;
using Refinanciacion.Application.Account.Models;
using Refinanciacion.Application.Account.Queries.GetAccountsList;
using System.Collections.Generic;
using Refinanciacion.Application.Account.Commands.UpdateAccountRol;

namespace Refinanciacion.Controllers
{

    public class AccountController : BaseController
    {

        [HttpPost("authenticate")]
        [AllowAnonymous]
        public async Task<object> Login([FromBody] LoginAccountCommand command)
        {
            return await Mediator.Send(command);
        }

        [HttpGet]
        [Authorize]
        public async Task<ActionResult<List<AccountLookupModel>>> Get()
        {
            return await Mediator.Send(new GetAccountsListQuery()); 
        }

        [HttpPut]
        [Authorize]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update([FromBody]UpdateAccountCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }

    }
}
