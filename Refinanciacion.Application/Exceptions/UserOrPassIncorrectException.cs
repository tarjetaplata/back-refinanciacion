﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Exceptions
{
    public class UserOrPassIncorrectException : Exception
    {
        public UserOrPassIncorrectException()
           : base($"Usuario o contraseña incorrectos")
        {
        }
    }
}
