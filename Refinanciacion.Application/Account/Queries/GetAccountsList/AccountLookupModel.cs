﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Account.Queries.GetAccountsList
{
    public class AccountLookupModel
    {
        public decimal? id { get; set; }
        public string userName { get; set; }
        public string sucursal { get; set; }
        public decimal rolId { get; set; }
    }
}
