﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Right.Queries.GetRightList
{
    public class RightLookupModel
    {
        public decimal Id { get; set; }
        public string Right { get; set; }
    }
}
