﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class LoanRefinancing
    {
        public decimal Id { get; set; }
        public decimal? LoanId { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal StatusId { get; set; }
        public decimal IdTakeOffLoan { get; set; }
        public decimal IdProduct { get; set; }
        public decimal? IdGrantRefinancing { get; set; }

        public virtual Product IdProductNavigation { get; set; }
        public virtual TakeOffLoan IdTakeOffLoanNavigation { get; set; }
        public virtual Status Status { get; set; }
    }
}
