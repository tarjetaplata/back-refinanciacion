﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Roles.Commands.UpdateRol
{
    public class UpdateRolCommand : IRequest
    {
        public decimal idRol;
        public decimal idRight;
    }
}
