﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Status
    {
        public Status()
        {
            Branch = new HashSet<Branch>();
            InstallmentChargeValidity = new HashSet<InstallmentChargeValidity>();
            InstallmentInterestsValidity = new HashSet<InstallmentInterestsValidity>();
            LoanParameters = new HashSet<LoanParameters>();
            LoanRefinancing = new HashSet<LoanRefinancing>();
            LoanTakeOffDebtValidity = new HashSet<LoanTakeOffDebtValidity>();
            Product = new HashSet<Product>();
            Rights = new HashSet<Rights>();
            Roles = new HashSet<Roles>();
            User = new HashSet<User>();
        }

        public decimal Id { get; set; }
        public string Mnemonic { get; set; }
        public string ShortDesc { get; set; }
        public string LongDesc { get; set; }
        public string StatusId { get; set; }
        public DateTime? StatusDate { get; set; }
        public decimal? StampUser { get; set; }
        public DateTime? StampDateTime { get; set; }

        public virtual ICollection<Branch> Branch { get; set; }
        public virtual ICollection<InstallmentChargeValidity> InstallmentChargeValidity { get; set; }
        public virtual ICollection<InstallmentInterestsValidity> InstallmentInterestsValidity { get; set; }
        public virtual ICollection<LoanParameters> LoanParameters { get; set; }
        public virtual ICollection<LoanRefinancing> LoanRefinancing { get; set; }
        public virtual ICollection<LoanTakeOffDebtValidity> LoanTakeOffDebtValidity { get; set; }
        public virtual ICollection<Product> Product { get; set; }
        public virtual ICollection<Rights> Rights { get; set; }
        public virtual ICollection<Roles> Roles { get; set; }
        public virtual ICollection<User> User { get; set; }
    }
}
