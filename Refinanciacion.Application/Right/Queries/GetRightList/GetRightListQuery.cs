﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Right.Queries.GetRightList
{
    public class GetRightListQuery : IRequest<List<RightLookupModel>>
    {
    }
}
