﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.InstallmentInterest.Commands.CreateInstallmentInterest;
using Refinanciacion.Application.InstallmentInterest.Commands.UpdateInstallmentInterest;
using Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList;
using Refinanciacion.Controllers;

namespace back_refinanciacion.Controllers
{
    public class InstallmentInterestController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<InstallmentInterestLookUpModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetInstallmentInterestListQuery()));
        }

        // POST: api/InstallmentInterest
        [HttpPost]
        public async Task<object> Create([FromBody]CreateInstallmentInterestCommand command)
        {
            return await Mediator.Send(command);
        }

        // PUT api/InstallmentInterest/5 // actualiza un InstallmentInterest con id = 5 (por ejemplo)
        [HttpPut("{id}")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update(string id, [FromBody]UpdateInstallmentInterestCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}
