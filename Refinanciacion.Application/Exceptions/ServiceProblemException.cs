﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.Exceptions
{
     public class ServiceProblemException : Exception
    {
        public ServiceProblemException(string service)
           : base($"Hay un problema con el servicio {service}. Consulte con el proveedor.")
        {
        }
    }
}
