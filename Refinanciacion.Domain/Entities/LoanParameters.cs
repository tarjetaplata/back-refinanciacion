﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class LoanParameters
    {
        public LoanParameters()
        {
            HistoricalParameters = new HashSet<HistoricalParameters>();
        }

        public decimal Id { get; set; }
        public decimal? MinDaysRefinancing { get; set; }
        public decimal? MaxDaysFirstInstallment { get; set; }
        public decimal? MaxDaysPaymentVerify { get; set; }
        public string FixedBranch { get; set; }
        public DateTime? StatusDate { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? StampUser { get; set; }
        public decimal StatusId { get; set; }

        public virtual Status Status { get; set; }
        public virtual ICollection<HistoricalParameters> HistoricalParameters { get; set; }
    }
}
