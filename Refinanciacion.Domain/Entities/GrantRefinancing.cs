﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class GrantRefinancing
    {
        public decimal Id { get; set; }
        public DateTime? StampDateTime { get; set; }
        public decimal? RefinancingNumber { get; set; }
        public string BranchRefinancing { get; set; }
        public string BranchUser { get; set; }
        public decimal? SumInstallment { get; set; }
        public decimal? InstallmentValueCapital { get; set; }
        public decimal? InteresPercent { get; set; }
        public decimal? InteresAmount { get; set; }
        public decimal? ContingencyFund { get; set; }
        public decimal? Assistance { get; set; }
        public decimal? Insurance { get; set; }
        public decimal? TotalAmountInstallment { get; set; }
        public DateTime? DueDateFirstInstallment { get; set; }
        public decimal? TotalAmountToPay { get; set; }
    }
}
