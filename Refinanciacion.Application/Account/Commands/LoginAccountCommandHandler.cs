﻿using System;
using System.Collections.Generic;
using System.Configuration;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using MediatR;
using Refinanciacion.Common;
using Refinanciacion.Persistence;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Infrastructure;
using Microsoft.Extensions.Options;
using Refinanciacion.Infrastructure.Security;
using Refinanciacion.Application.Account.Models;

namespace Refinanciacion.Application.Account.Commands
{
    public class LoginAccountCommandHandler : IRequestHandler<LoginAccountCommand, object>
    {
        private readonly ModelContext _context;
        private readonly AppSettings _appSettings;
        private readonly IJwtTokenGenerator _jwtTokenGenerator;

        public LoginAccountCommandHandler(ModelContext context, 
            IOptions<AppSettings> appSettings,
            IJwtTokenGenerator jwtTokenGenerator)
        {
            _context = context;
            _appSettings = appSettings.Value;
            _jwtTokenGenerator = jwtTokenGenerator;
        }

        public async Task<object> Handle(LoginAccountCommand request, CancellationToken cancellationToken)
        {
            // 1.- validacion de usuario contra los webservices de tarjeta plata

            bool result; 

            try { 

                result = UtilLoginTP.CallLoginWebService(request.UserName, request.Password, _appSettings.externalServices.UrlLoginTP);

            } catch {

                throw new ServiceProblemException("Login Targeta Plata");
            }
            
            if (result)
            {
                // 2.- Buscamos el usuario en la base de datos de objetivos por nombre de usuario 
                var user = await _context.User.SingleOrDefaultAsync(x => x.Name == request.UserName);

                if (user == null)
                {
                    // No existe el usuario en base de datos. Preguntar esto si por default se crean los usuarios o no
                    return new LoginAccountStatusDto("error", "El usuario no esta cargado en la base de datos de Refinanciacion.");

                } else {

                    // Buscamos el estado del usuario. Tiene que ser vigente para continuar de lo contrario devuelve Exception
                    var status = await _context.Status.SingleOrDefaultAsync(x => x.Id == user.StatusId);

                    // Si el usuario no esta vigente se devuelve Exception
                    if (status.ShortDesc != Refinanciacion.Infrastructure.Status.Vigente)
                    {
                        throw new StatusException(nameof(Refinanciacion.Domain.Entities.Status), status.ShortDesc);
                    }

                    user.LastEntryDate = DateTime.Now;

                    await _context.SaveChangesAsync(cancellationToken);

                    // 3.- Buscamos la sucursal del usuario para devolverla 
                    var branch = await _context.Branch.SingleOrDefaultAsync(x => x.Id == user.BranchId);

                    // 3.1 .- Buscamos el rol del usuario
                    var rol = await _context.Roles.SingleOrDefaultAsync(x => x.Id == user.RolesId);

                    // 4.- Buscamos los permisos de ese rol
                    var rights = await _context.RolesRights.Where(x => x.RolesId == user.RolesId).ToListAsync();
                    
                    List<string> listRights = new List<string>();

                    rights.ForEach(async x =>
                    {
                        var right = await _context.Rights.Where(y => y.Id == x.RightsId).FirstOrDefaultAsync();
                        listRights.Add(right.ShortDesc);
                    });

                    // 5.- Generamos el token del usuario 
                    string token = await _jwtTokenGenerator.CreateTokenAsync(user.Name, rol.ShortDesc);

                    // 6.- Creamos el objeto DTO para devolver al front
                    return new UserDto(user.Name,
                        rol.ShortDesc,
                        token,
                        branch.ShortDesc,
                        listRights);
                }

            } else {

                return new LoginAccountStatusDto("error", "Usuario y/o contraseña incorrectos.");
            }
        }
    }
}
