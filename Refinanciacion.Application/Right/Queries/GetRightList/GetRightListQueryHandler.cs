﻿using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.Right.Queries.GetRightList
{
    public class GetRightListQueryHandler : IRequestHandler<GetRightListQuery, List<RightLookupModel>>
    {
        private readonly ModelContext _context;

        public GetRightListQueryHandler(ModelContext context)
        {
            _context = context;
        }

        public async Task<List<RightLookupModel>> Handle(GetRightListQuery request, CancellationToken cancellationToken)
        {
            var roles = await _context.Rights
               .ToListAsync();
            
            List<RightLookupModel> listRightLookUpModel = new List<RightLookupModel>();

            roles.ForEach(x =>
            {
                RightLookupModel rightLookupModel = new RightLookupModel();
                rightLookupModel.Id = x.Id;
                rightLookupModel.Right = x.ShortDesc;

                listRightLookUpModel.Add(rightLookupModel);
            });

            return listRightLookUpModel;

        }
    }
}
