﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class Action
    {
        public Action()
        {
            HistoricalParameters = new HashSet<HistoricalParameters>();
        }

        public decimal Id { get; set; }
        public string ShortDesc { get; set; }
        public string Type { get; set; }

        public virtual ICollection<HistoricalParameters> HistoricalParameters { get; set; }
    }
}
