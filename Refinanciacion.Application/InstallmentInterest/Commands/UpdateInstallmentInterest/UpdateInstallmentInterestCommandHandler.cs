﻿using System;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;

namespace Refinanciacion.Application.InstallmentInterest.Commands.UpdateInstallmentInterest
{
    public class UpdateInstallmentInterestCommandHandler : IRequestHandler<UpdateInstallmentInterestCommand, Unit>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;

        public UpdateInstallmentInterestCommandHandler(ModelContext context,
            IMediator mediator, ICurrentUserAccessor currentUserAccessor)
        {
            _currentUserAccessor = currentUserAccessor;
            _context = context;
        }

        public async Task<Unit> Handle(UpdateInstallmentInterestCommand request, CancellationToken cancellationToken)
        {
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());

            var entity = await _context.InstallmentInterestsValidity
                .SingleAsync(c => c.Id == request.id, cancellationToken);


            if (entity == null)
            {
                throw new NotFoundException(nameof(InstallmentInterestsValidity), request.id);
            }

            entity.StatusId = 2;
            entity.StatusDate = DateTime.Now;
            entity.StampUser = user.Id;

            ICollection<Domain.Entities.InstallmentInterests> collectionInstallmentInterest = null;

            request.listInstallmentInterest.ForEach(x => {
                Domain.Entities.InstallmentInterests InstallmentInterest = new Domain.Entities.InstallmentInterests();
                InstallmentInterest.Installment = x.installment;
                InstallmentInterest.Interests = x.interest;
                
                collectionInstallmentInterest.Add(InstallmentInterest);
            });

            // Creamos el objeto a aguardar
            var newEntity = new InstallmentInterestsValidity
            {
                FromDate = request.fromDate,
                ToDate = request.toDate,
                StampDateTime = DateTime.Now.Date,
                StampUser = user.Id,
                InstallmentInterests = collectionInstallmentInterest
            };

            _context.InstallmentInterestsValidity.Update(entity);
            _context.InstallmentInterestsValidity.Add(newEntity);
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;
        }
    }
}
