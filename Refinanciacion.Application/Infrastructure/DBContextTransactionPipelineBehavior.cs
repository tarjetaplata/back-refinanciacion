﻿using MediatR;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.Extensions.Logging;


namespace Refinanciacion.Application.Infrastructure
{
    public class DBContextTransactionPipelineBehavior<TRequest, TResponse> : IPipelineBehavior<TRequest, TResponse>
    {
        private readonly ModelContext _context;
        private readonly ILogger _logger;

        public DBContextTransactionPipelineBehavior(ModelContext context, ILogger<TRequest> logger)
        {
            _logger = logger;
            _context = context;
        }

        public async Task<TResponse> Handle(TRequest request, CancellationToken cancellationToken, RequestHandlerDelegate<TResponse> next)
        {
            TResponse result = default(TResponse);
            var name = typeof(TRequest).Name;

            try {
                //_context.Database.BeginTransaction();
                _logger.LogInformation("Objetivos BEGIN TRANSACTION: {Name} {@Request}", name, request);

                result = await next();

                //_context.Database.CommitTransaction();
                _logger.LogInformation("Objetivos COMMIT TRANSACTION: {Name} {@Request}", name, request);
            }
            catch (Exception)
            {
                //_context.Database.RollbackTransaction();
                _logger.LogInformation("Objetivos ROLLBACK: {Name} {@Request}", name, request);
                throw;
            }

            return result;
        }
    }
}
