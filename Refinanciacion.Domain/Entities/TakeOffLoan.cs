﻿using System;
using System.Collections.Generic;

namespace Refinanciacion.Domain.Entities
{
    public partial class TakeOffLoan
    {
        public TakeOffLoan()
        {
            LoanRefinancing = new HashSet<LoanRefinancing>();
        }

        public decimal Id { get; set; }
        public decimal? Capital { get; set; }
        public decimal? Interes { get; set; }
        public decimal? Punitorio { get; set; }
        public decimal? Mora { get; set; }
        public decimal IdUser { get; set; }

        public virtual User IdUserNavigation { get; set; }
        public virtual ICollection<LoanRefinancing> LoanRefinancing { get; set; }
    }
}
