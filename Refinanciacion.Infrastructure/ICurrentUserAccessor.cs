﻿using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Infrastructure
{
    public interface ICurrentUserAccessor
    {
        string GetCurrentUsername();
    }
}
