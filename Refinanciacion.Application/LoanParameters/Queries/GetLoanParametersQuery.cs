﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.LoanParameters.Queries
{
    public class GetLoanParametersQuery : IRequest<LoanParametersLookupModel>
    {
    }
}
