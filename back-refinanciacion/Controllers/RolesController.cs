﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.Roles.Commands.UpdateRol;
using Refinanciacion.Application.Roles.Queries.GetRolesRightList;
using Refinanciacion.Application.Roles.Queries.GetRolessList;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace Refinanciacion.Controllers
{
    [Authorize]
    public class RolesController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<RolesLookupModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetRolesListQuery()));
        }

        [HttpGet("right")]
        public async Task<ActionResult<List<RolesRightLookUpModel>>> GetRightRelation()
        {
            return Ok(await Mediator.Send(new GetRolesRightListQuery()));
        }

        // PUT api/roless/5 // actualiza un target con id = 5 (por ejemplo)
        [HttpPut("right")]
        [ProducesResponseType(StatusCodes.Status204NoContent)]
        [ProducesDefaultResponseType]
        public async Task<IActionResult> Update([FromBody]UpdateRolCommand command)
        {
            await Mediator.Send(command);

            return NoContent();
        }
    }
}
