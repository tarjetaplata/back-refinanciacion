﻿using MediatR;
using System;
using System.Collections.Generic;
using System.Text;

namespace Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList
{
    public class GetInstallmentInterestListQuery : IRequest<List<InstallmentInterestLookUpModel>>
    {
    }
}