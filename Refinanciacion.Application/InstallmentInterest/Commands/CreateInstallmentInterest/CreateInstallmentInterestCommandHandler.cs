﻿using System;
using MediatR;
using Microsoft.EntityFrameworkCore;
using Refinanciacion.Application.Exceptions;
using Refinanciacion.Domain.Entities;
using Refinanciacion.Infrastructure;
using Refinanciacion.Persistence;
using System;
using System.Collections.Generic;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using Refinanciacion.Application.InstallmentInterest.Commands.CreateInstallmentInterest;
using Refinanciacion.Application.InstallmentInterest.Queries.GetInstallmentInterestList;

namespace Refinanciacion.Application.InstallmentInterest.Commands.CreateInstallmentInterest
{
    public class CreateInstallmentInterestCommandHandler : IRequestHandler<CreateInstallmentInterestCommand, object>
    {
        private readonly ModelContext _context;
        private readonly ICurrentUserAccessor _currentUserAccessor;

        public CreateInstallmentInterestCommandHandler(ModelContext context, ICurrentUserAccessor currentUserAccessor)
        {
            _context = context;
            _currentUserAccessor = currentUserAccessor;
        }

        public async Task<object> Handle(CreateInstallmentInterestCommand request, CancellationToken cancellationToken)
        {
            // Traemos el usuario que da el alta. Podemos simplificar guardando los datos del usuario en el JWT para no da.
            // Tienen que estar las APIS en Authorize para que funcione asi tenemos la session del usuario.
            // Con esto hacer el alta del history 
            var user = await _context.User.SingleOrDefaultAsync(x => x.Name == _currentUserAccessor.GetCurrentUsername());

            if (user == null)
                throw new NotFoundException(nameof(User), "usuario");


            ICollection<Domain.Entities.InstallmentInterests> collectionInstallmentInterest = null;

            request.listInstallmentInterest.ForEach(x => {
                Domain.Entities.InstallmentInterests InstallmentInterest = new Domain.Entities.InstallmentInterests();
                InstallmentInterest.Installment = x.installment;
                InstallmentInterest.Interests = x.interest;

                collectionInstallmentInterest.Add(InstallmentInterest);
            });

            // Creamos el objeto a aguardar
            var entity = new InstallmentInterestsValidity
            {
                FromDate = request.fromDate,
                ToDate = request.toDate,
                StampDateTime = DateTime.Now.Date,
                StampUser = user.Id,
                InstallmentInterests = collectionInstallmentInterest
            };

            // lo guardamos en la tabla
            await _context.SaveChangesAsync(cancellationToken);

            return Unit.Value;

        }


    }
}
