﻿using System;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;

namespace Refinanciacion.Infrastructure.Security
{
    public class JwtTokenGenerator : IJwtTokenGenerator
    {
        private readonly AppSettings _appSettings;
        private readonly JwtIssuerOptions _jwtOptions;

        public JwtTokenGenerator(IOptions<AppSettings> appSettings,
            IOptions<JwtIssuerOptions> jwtOptions)
        {
            _appSettings = appSettings.Value;
            _jwtOptions = jwtOptions.Value;
        }

        public async Task<string> CreateTokenAsync(string username, string rol)
        {
            var claims = new[]
            {
               new Claim(JwtRegisteredClaimNames.Sub, username),
               new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
               new Claim(JwtRegisteredClaimNames.Iat,
                    new DateTimeOffset(_jwtOptions.IssuedAt).ToUnixTimeSeconds().ToString(),
                    ClaimValueTypes.Integer64),
                new Claim(ClaimTypes.Role, rol)
            };
            var jwt = new JwtSecurityToken(
                    _jwtOptions.Issuer,
                    _jwtOptions.Audience,
                    claims,
                    _jwtOptions.NotBefore,
                    _jwtOptions.Expiration,
                    _jwtOptions.SigningCredentials);

            var encodedJwt = new JwtSecurityTokenHandler().WriteToken(jwt);
            return encodedJwt;

        }
    }
}
