﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Refinanciacion.Application.Right.Queries.GetRightList;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Refinanciacion.Controllers
{
    [Authorize]
    public class RightController : BaseController
    {
        [HttpGet]
        public async Task<ActionResult<List<RightLookupModel>>> Get()
        {
            return Ok(await Mediator.Send(new GetRightListQuery()));
        }
    }
}
